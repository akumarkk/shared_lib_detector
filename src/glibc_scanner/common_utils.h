#ifndef __COMMON_UTILS__
#define __COMMON_UTILS__

#define DEBUG_ENABLED 1

#define debug(fmt,...)\
    do{\
	if(DEBUG_ENABLED) {\
	    FILE *fp = fopen("./scanner.logs", "a");\
	    fprintf(fp, "%s(%d) :	" fmt "\n", __FUNCTION__, __LINE__, __VA_ARGS__);\
	    fclose(fp);\
	}\
    }while(0)


#define stamp_time(fmt,...)\
    do{\
        if(DEBUG_ENABLED) {\
            FILE *fp = fopen("./scanner_time.logs", "a");\
            fprintf(fp, "%s(%d) :       " fmt "\n", __FUNCTION__, __LINE__, __VA_ARGS__);\
            fclose(fp);\
        }\
    }while(0)

#endif
