#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <sys/user.h>
#include <sys/ptrace.h>
#include <inttypes.h>
#include <signal.h>
#include <argp.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include "config.h"
#include "log.h"
#include "dwdebug.h"
#include "target_api.h"
#include "target.h"
#include "probe_api.h"
#include "probe.h"
#include "alist.h"
#include "list.h"

#include "sho_detector_utils.h"
#include "sho_detector_main.h"
#include "common_utils.h"

#include "target_api.h"
#include "target.h"
#include "glib_wrapper.h"

#ifdef HAVE_CLIPSSRC
#include "clips.h"
#else
#include <clips/clips.h>
#endif

/* These hard-coded macros must be read from command-line */
#define DEBUG_FILE_ROOT_PREFIX	    "/mnt/extra/debug"
#define VERSION_SCANNER_READ_SIZE  0x1000 


/* All Global variables */
vscanner_argp_state_t	opts;
struct		target	*target = NULL;
char			**sys_call_names = NULL;
unsigned long		*sys_call_table  = NULL;
unsigned long		**function_prologue = NULL;
char			base_fact_file[100];
ADDR			syscall_table_vm;

struct argp_option vscanner_argp_opts[] = {
    { "vscanner-log-file",     VSCANNER_ARGP_LOGFILE,        "FILE",	0,  "Log file for hex data. Default to ./vscanner_hex.log",	0},
    { "vscanner-olay-process", VSCANNER_ARGP_OLAY_PROCESS,    "PROCESS",	0,  "Target overlay process",	0},
    { "vscanner-olay-library", VSCANNER_ARGP_OLAY_LIBRARY,    "LIBRARY",	0,  "Target overlay library",	0},
    { 0,0,0,0,0,0 },
};

struct argp vscanner_argp = {
    vscanner_argp_opts,	vscanner_argp_parse_opt,NULL,NULL,NULL,NULL,NULL, };


error_t 
vscanner_argp_parse_opt(
	int		    key,
	char		    *arg,
	struct argp_state   *state) 
{
    struct vscanner_argp_state *opts = \
        (struct vscanner_argp_state *)target_argp_driver_state(state);

    debug("parsing arguments ...%s", "");
    switch (key) 
    {
    
	case ARGP_KEY_ARG:
        return ARGP_ERR_UNKNOWN;
    
	case ARGP_KEY_ARGS:
        /* Eat all the remaining args. */
	if (state->quoted > 0)
            opts->argc = state->quoted - state->next;
        else
            opts->argc = state->argc - state->next;
        if (opts->argc > 0) {
            opts->argv = calloc(opts->argc,sizeof(char *));
            memcpy(opts->argv,&state->argv[state->next],opts->argc*sizeof(char *));
            state->next += opts->argc;
        }
        return 0;
    
	case ARGP_KEY_INIT:
        target_driver_argp_init_children(state);
        return 0;
    
	case ARGP_KEY_END:
	case ARGP_KEY_NO_ARGS:
	case ARGP_KEY_SUCCESS:
	    opts->tspec = target_argp_target_spec(state);
	    return 0;
    
	case ARGP_KEY_ERROR:
	case ARGP_KEY_FINI:
	    return 0;

	case VSCANNER_ARGP_LOGFILE:
	    debug("parsed log file = %s", arg);
	    strcpy(opts->logfile, arg);
	    break;

	case VSCANNER_ARGP_OLAY_PROCESS:
	    debug("overlay process = %s", arg);
	    strcpy(opts->olay_process, arg);
	    break;

	case VSCANNER_ARGP_OLAY_LIBRARY:
	     debug("overlay library = %s", arg);
	     strcpy(opts->olay_library, arg);

	     if(strcmp(arg, "libc") == 0)
	     {
		  strcpy(opts->olay_process, "init");
	     }
	     break;
    
	default:
	    return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

target_status_t cleanup() 
{
    target_status_t retval;
    target_pause(target);
    retval = target_close(target);
    target_finalize(target);
    return retval;
}

void 
sigh(int signo) 
{
    if (target) {
        target_pause(target);
        fprintf(stderr,"Ending monitoring on signal %d.\n",signo);
        cleanup();
        fprintf(stderr,"Ended monitoring.\n");
    }
    exit(0);
}

int
get_shared_objects(void)
{
    int			    result = 0;
    target_status_t	    status;
    static struct timeval   tm1;
    static struct timeval   tm2;
    unsigned long long	    t;

    if (opts.dump_timing)
        gettimeofday(&tm1, NULL);
    
    /* Pause the target */
    if ((status = target_status(target)) != TSTATUS_PAUSED) {
        if (target_pause(target)) {
                fprintf(stderr,"ERROR: Failed to pause the target \n");
                result = 1;
                goto resume;
         }
    }

    if (opts.dump_timing) {
        gettimeofday(&tm2, NULL);
        timersub(&tm2, &tm1, &tm2);
        t = (1000 * tm2.tv_sec + tm2.tv_usec / 1000);
        fprintf(stdout,"INFO: Time taken to pause the target is %llu ms\n", t);
    }
    if (opts.dump_timing)
        gettimeofday(&tm1, NULL);

    result = object_info(target);
    if(result)
    {
	fprintf(stderr,"ERROR: object_info failed.\n");
	result  = 1;
	goto resume;
    }
    
    if (opts.dump_timing) {
        gettimeofday(&tm2, NULL);
        timersub(&tm2, &tm1, &tm2);
        t = (1000 * tm2.tv_sec + tm2.tv_usec / 1000);
        fprintf(stdout,"INFO: Time taken to get file info is %llu ms\n", t);
    }

resume:
    if (opts.dump_timing)
        gettimeofday(&tm1, NULL);
    if ((status = target_status(target)) == TSTATUS_PAUSED) {
        if (target_resume(target)) {
            
        }
    }
    if (opts.dump_timing) {
        gettimeofday(&tm2, NULL);
        timersub(&tm2, &tm1, &tm2);
        t = (1000 * tm2.tv_sec + tm2.tv_usec / 1000);
        fprintf(stdout,"INFO: Time taken to resume the target is %llu ms\n", t);
    }

    return result;
}

void
register_signal_handlers(void)
{
    signal(SIGHUP,sigh);
    signal(SIGINT,sigh);
    signal(SIGQUIT,sigh);
    signal(SIGABRT,sigh);
    signal(SIGSEGV,sigh);
    signal(SIGPIPE,sigh);
    signal(SIGALRM,sigh);
    signal(SIGTERM,sigh);
    signal(SIGUSR1,sigh);
    signal(SIGUSR2,sigh);
}



/* converts command line arguments into proper version 
 * scanner arguments
 */
void
vscanner_args_init(
	vscanner_argp_state_t	*opts,
	vscanner_arguments_t	*vsnr_args)
{
    if(opts->logfile)
	strcpy(vsnr_args->logfile, opts->logfile);

    strcpy(vsnr_args->olay_library, opts->olay_library);
    strcpy(vsnr_args->olay_process, opts->olay_process);
}


int
main(int argc, char** argv)
{
    target_status_t         	status;
    int			        result		=   0;
    int			        retry		=   3;
    tid_t			tid		=   0;
    struct target_spec		*tspec		=   NULL;
    struct target_spec		*overlay_spec   =   NULL;
    struct target		*overlay_target =   NULL;
    struct probe		*probe		=   NULL;
    char		        targetstr[80];
    target_status_t		tstat;
    vscanner_arguments_t	vsnr_args;
    struct timespec 		start, end, elapsed_time;
    clock_t			t_start, t_end;

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
    t_start = clock();

    memset(&opts,0,sizeof(opts));
    tspec = target_argp_driver_parse_one(&vscanner_argp, &opts, argc, argv,
					 TARGET_TYPE_XEN | TARGET_TYPE_GDB,1);
    if (!tspec)
    {
	fprintf(stderr,"ERROR: Could not parse target arguments!\n");
	exit(-1);
    }

    register_signal_handlers();
    dwdebug_init();
    target_init();
    atexit(target_fini);
    atexit(dwdebug_fini);
    vscanner_args_init(&opts, &vsnr_args);

    target = target_instantiate(tspec,NULL);
    if (!target)
    {
	fprintf(stderr,"ERROR: Could not instantiate target!\n");
	exit(0);
    }

    if (target_open(target))
    {
	fprintf(stderr,"ERROR: Could not open %s!\n",targetstr);
	exit(0);
    }

    InitializeEnvironment();
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
    t_end = clock();
    elapsed_time = diff(start, end);
    stamp_time("Initialization time = [%lu] second [%lu] nanoseconds", elapsed_time.tv_sec, elapsed_time.tv_nsec);
    //stamp_time("Initialization time = [%lf] second ", ((double)(t_end - t_start))/CLOCKS_PER_SEC);

    debug("***************************** VERSION SCANNER STARTED ****************************%S\n", "");
    fflush(stderr);
    fflush(stdout);

    while(retry)
    {
	debug("retry count = [%d]", retry);

	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
	result = vmi_version_scanner(target, &vsnr_args);
	
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
	elapsed_time = diff(start, end);
	stamp_time("vmi_version_scanner time = [%lu] second [%lu] nanoseconds", elapsed_time.tv_sec, elapsed_time.tv_nsec);

	if(result == 0)
	    break;

	retry--;
	sleep(1);
    }

#if 0
    while(1)
    {
	tstat = target_monitor(target);

	fflush(stderr);
	fflush(stdout);
	
	//tstat = cleanup();
	if(tstat == TSTATUS_DONE) {
	    fprintf(stdout, " Monitoring finished.\n");
	    return 0;
	}
	else if (tstat == TSTATUS_ERROR) {
	    fprintf(stdout, "Monitoring failed!\n");
	    return 1;
	}
	else {
	    fprintf(stdout, "Monitoring failed with %d!\n",tstat);
	    return 1;
	}
    }
#endif

    return 0;
}
