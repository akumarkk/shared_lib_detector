#ifndef __SHO_DETECTOR_UTILS__
#define __SHO_DETECTOR_UTILS__

#include "target_api.h"
#include "target.h"
#include "glib_wrapper.h"
#include <time.h>

#define NSEC_PER_SEC    1000000000.0
#define HZ 100

#define VALUE_FREE(v)   { value_free(v); v = NULL; }

/* Macros for caomputing the CPU LOAD */
#define FSHIFT 11
#define FIXED_1 (1<<FSHIFT)
#define LOAD_INT(x) ((x) >> FSHIFT)
#define LOAD_FRAC(x) LOAD_INT(((x) & (FIXED_1-1)) * 100)


#ifndef S_IFMT
/* Macros for distuinguishing between different kinds of files */
#define S_IFMT  00170000
#define S_IFSOCK 0140000
#define S_IFLNK  0120000
#define S_IFREG  0100000
#define S_IFBLK  0060000
#define S_IFDIR  0040000
#define S_IFCHR  0020000
#define S_IFIFO  0010000

#define S_ISLNK(m)      (((m) & S_IFMT) == S_IFLNK)
#define S_ISREG(m)      (((m) & S_IFMT) == S_IFREG)
#define S_ISDIR(m)      (((m) & S_IFMT) == S_IFDIR)
#define S_ISCHR(m)      (((m) & S_IFMT) == S_IFCHR)
#define S_ISBLK(m)      (((m) & S_IFMT) == S_IFBLK)
#define S_ISFIFO(m)     (((m) & S_IFMT) == S_IFIFO)
#define S_ISSOCK(m)     (((m) & S_IFMT) == S_IFSOCK)
#endif

/* Macros to covert between priority and nice values*/
#define LINUX_MAX_RT_PRIO 100
#define LINUX_NICE_TO_PRIO(nice)      (LINUX_MAX_RT_PRIO + (nice) + 20)
#define LINUX_PRIO_TO_NICE(prio)      ((prio) - LINUX_MAX_RT_PRIO - 20)

/* Per proces flags */
#define LINUX_PF_VCPU         0x00000010  
#define LINUX_PF_WQ_WORKER    0x00000020
#define LINUX_PF_SUPERPRIV    0x00000100
#define LINUX_PF_KTHREAD      0x00200000
#define LINUX_PF_KSWAPD       0x00040000

// Define any other arhguments that needs to be passed to vscanner
typedef struct vscanner_arguments_
{
    char	olay_process[NAME_MAX];
    char	olay_library[NAME_MAX];
    char	logfile[NAME_MAX];

}vscanner_arguments_t;

unsigned long div_u64(unsigned long dividend, unsigned int divisor);
unsigned long div64_u64(unsigned long dividend, unsigned long divisor);
unsigned long nsec_to_jiffies(unsigned long n);
unsigned long scale_utime(unsigned long utime, unsigned long rtime, unsigned long total);


int get_shared_objects(void);
int object_info(struct target *target);
int gather_object_info(struct target *target, struct value *value, void *data);
int generate_snapshot();
int commandline_info();
int gather_commandline_info(struct target *target, struct value *value, void *data);
void
version_scanner(struct target *target,
		vscanner_arguments_t *vsnr_args);

struct timespec
diff(struct timespec start, struct timespec end);

struct target *
get_overlay_target(struct target *target, char *process);


#define dump_vscanner_arguments(args)\
do{\
    if(args == NULL)\
    {\
        debug("vscanner_arguments is NULL %s", "");\
        return;\
    }\
    \
    debug("***************************************************** %s", "");\
    debug("logfile      :       %s", args->logfile);\
    debug("olay_library :       %s", args->olay_library);\
    debug("olay_process :       %s", args->olay_process);\
    debug("***************************************************** %s", "");\
}while(0)


#endif //__SHO_DETECTOR_UTILS__
