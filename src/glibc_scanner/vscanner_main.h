#ifndef __SHO_DETECTOR_MAIN_H__
#define __SHO_DETECTOR_MAIN_H__

#define VSCANNER_ARGP_LOGFILE    0x474747
#define VSCANNER_ARGP_OLAY_PROCESS    0x474748
#define VSCANNER_ARGP_OLAY_LIBRARY    0x474749

typedef struct vscanner_argp_state
{
    char    logfile[NAME_MAX];
    char    olay_library[NAME_MAX];
    char    olay_process[256];

    int	    dump_debug;
    int	    dump_timing;
    int	    argc;
    char    **argv;
    /* Grab this from the child parser. */
    struct target_spec *tspec;
}vscanner_argp_state_t;


error_t
vscanner_argp_parse_opt(int key, char *arg, struct argp_state *state);

void
vscanner_args_init(
        vscanner_argp_state_t     *opts,
        vscanner_arguments_t      *vsnr_args);

#endif //__SHO_DETECTOR_MAIN__
