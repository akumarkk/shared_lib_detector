#ifndef __COMMON_UTILS__
#define __COMMON_UTILS__

#define DEBUG_ENABLED 1

#define debug(fmt,...)\
    do{\
	if(DEBUG_ENABLED) {\
	    fprintf(stdout, "%s(%d) :	" fmt "\n", __FUNCTION__, __LINE__, __VA_ARGS__);\
	}\
    }while(0)

#endif
