
Shared Object Detector 
    It is used to report the version number of the shared object linked to particular binary in the VM.

NOTE : Most of the code here is taken from vmi/asm/moti/policy\_engine.c and vmi/asm/moti/policy\_engine.h of A3 project.

    sho_detector_main.*		Main binary to get the shared library version
    sho_detector_utils.*	utility functions required to read the version of the shared object
