#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define VERSION_STRING "4f70656e535348"
#define VERSION_SCANNER_RDWR_SIZE 1024

char global_ver_desc[256];
int  global_major, global_minor;


void
potential_version_string(char *version_str)
{
    int	    major=0, minor=0;
    char    ver_desc[256] = "";

    sscanf(version_str, "OpenSSH_%d.%d.%s", &major, &minor, ver_desc);

    //printf("version_str = %s	major = %d  minor = %d  ver_desc = %s\n", version_str, major, minor, ver_desc);
    if(global_major < major || ((global_major == major) && (global_minor < minor)))
    {
	global_major = major;
	global_minor = minor;
	strcpy(global_ver_desc, ver_desc);
    }
}



/* convert hex version string in ASCII */
void
version_str_print(char *version_str)
{
    int	    index=0, sindex=0;
    long    int_val;
    char    ch;
    char    hex[3] = "";
    char    str[256] = "";

    //printf("version string = %s\n\n", version_str);
    //fflush(stdout);

    while(version_str[index])
    {
	hex[0] = version_str[index++];
	hex[1] = version_str[index++];
	hex[2] = '\0';

	ch = (char)strtol(hex, NULL, 16);
	str[sindex++] = ch;
    }
    str[sindex] = '\0';
    printf("%s\n", str);
    potential_version_string(str);
}


int
main(int argc, char *argv[])
{
    FILE    *fp = NULL;
    char    buf[VERSION_SCANNER_RDWR_SIZE+1] = "";
    char    version_str[VERSION_SCANNER_RDWR_SIZE] = "";
    int	    index, nbytes;
    char    *buf_ptr = NULL, *start = NULL;


    if(argc < 2)
    {
	printf("usage : extract_version_string <data_file>\n");
	exit(-1);
    }

    fp = fopen(argv[1], "r");
    if(fp == NULL)
    {
	printf("opening [%s] failed\n", argv[1]);
	exit(-1);
    }

    while(!feof(fp))
    {
	memset(buf, 0, VERSION_SCANNER_RDWR_SIZE);
	nbytes = fread(buf, 1, VERSION_SCANNER_RDWR_SIZE, fp);
	buf[nbytes] = '\0';

	buf_ptr = buf;

	while(start = strstr(buf_ptr, VERSION_STRING))
	{
	    //printf("found string @%s\n", start);
	    //fflush(stdout);
	    index = 0;
	    while(!isspace(start[index]))
	    {
		version_str[index] = start[index];
		index++;
	    }

	    version_str[index] = '\0';
	    version_str_print(version_str);
	    buf_ptr = buf_ptr + index;
	}
    }

    printf("\n***********************************************************************\n");
    printf("*           version  =  OpenSSH_%d.%d.%s                               \n", global_major, global_minor, global_ver_desc);
    printf("***********************************************************************\n");

    printf("exiting ...\n");
    return 0;
}



