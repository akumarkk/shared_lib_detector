#include "target_os.h"
#include "target_os_linux_generic.h"
#include <unistd.h>
#include "sho_detector_utils.h"
#include "common_utils.h"

/* These hard-coded macros must be read from command-line */
#define DEBUG_FILE_ROOT_PREFIX      "/mnt/extra/debug"
#define VERSION_SCANNER_LOGFILE     "/mnt/extra/logs/icu_version_scanner_fixed.log"
#define SCANNER_TARGET_FILENAME "sshd.c"
#define SCANNER_TARGET_FILE_LINE 1199

#define VERSION_SCANNER_READ_SIZE  0x1000

/* We are interested in monitoring certain programs, not all the programs.
 *   programs_to_monitor[] : lists all the programs we are interested in
 */
char *programs_to_monitor[] = {"sshd", "whoopsie", "dnsmasq"};

extern char		base_fact_file[100];
extern unsigned long	*sys_call_table;
extern char		**sys_call_names;
extern unsigned long	**function_prologue;


unsigned long  div_u64(unsigned long dividend, unsigned int divisor) 
{
        return dividend / divisor;
}

unsigned long div64_u64( unsigned long dividend, unsigned long divisor) 
{
        return dividend / divisor;
}

unsigned long nsec_to_jiffies(unsigned long n) 
{
        return div_u64(n, NSEC_PER_SEC/HZ);
}

unsigned long scale_utime(unsigned long utime, unsigned long rtime, unsigned long total) 
{
    unsigned long temp;
    temp = (unsigned long )rtime;
    temp *= (unsigned long) utime;
    temp = div64_u64(temp, (unsigned long) total);
    return (unsigned long) temp;
}

void
version_scanner(struct target *overlay_target,
                struct target *target)
{
    GList                       *l1,*l2,*l3;
    struct addrspace            *space      =   NULL;
    struct memregion            *region     =   NULL;
    struct memrange             *range      =   NULL;
    unsigned char               *ret        =   NULL;
    unsigned char               buff[VERSION_SCANNER_READ_SIZE] = "";
    unsigned long long          start_addr = 0;
    unsigned long long          v_start_addr = 0, p_start_addr = 0;
    int                         i;
    tid_t                       tid;
    FILE                        *fp         =   NULL;

    debug("reading the address space %s", "");
    debug("SCANNER base_target             =            %p    ", target);
    debug("SCANNER overlay_target             =         %p    ", overlay_target);
    tid = target_gettid(overlay_target);
    debug("Scanning tid [%d]", tid);

    fp = fopen(VERSION_SCANNER_LOGFILE, "w");
    if(fp == NULL)
    {
        fprintf(stderr, "ERROR: opening file %s failed !!!\n", VERSION_SCANNER_LOGFILE);
        exit(-1);
    }

    debug("reading the address space %s", "");
    v_g_list_foreach(overlay_target->spaces, l1, space)
    {
        v_g_list_foreach(space->regions,l2,region)
        {
            v_g_list_foreach(region->ranges,l3,range)
            {
                printf("start = %lx, end = %lx\n",range->start,range->end);

                for(start_addr=range->start; start_addr<range->end; start_addr+=(0x1ull<<12))
                {
                    v_start_addr = start_addr;
                    memset(buff, 0, VERSION_SCANNER_READ_SIZE);
                    if(target_addr_v2p(target, tid, v_start_addr, &p_start_addr))
                    {
                         fprintf(stdout,"SCANNER_ERROR: could not translate virtual address 0x%"PRIxADDR"\n", v_start_addr);
                         continue;
                    }
                    fprintf(stdout, "Successfully converted vaddr to phy_addr\n");

                    ret = target_read_physaddr(target, p_start_addr, VERSION_SCANNER_READ_SIZE, buff);
                    if(ret == NULL)
                    {
                        fprintf(stderr, "ERROR: target_read_addr failed - %s\n", strerror(errno));
                        continue ;
                    }

                    for(i=0; i<VERSION_SCANNER_READ_SIZE; i++)
                    {
                        fprintf(fp, "%2x", buff[i]);
                    }
                    fprintf(fp, "\n");
                    fflush(fp);
                }
            }
        }
    }
    fclose(fp);
}

int generate_timestamp(char *date) 
{
    time_t 	t;
    struct tm 	*tm;
    int 	result = 0;

    time(&t);
    tm = localtime(&t);
    result = strftime(date,100, "state_information/%Y_%m_%d_%H_%M_%S.fac", tm);

    return result;
}

int save_sys_call_table_entries(
	struct target	*target,
	int		dump_debug, 
	ADDR		syscall_table_vm) 
{
    int	    i, max_num;
    unsigned char *res = NULL;
    struct target_os_syscall *sc;
    unsigned char  prologue[16];
    struct bsymbol *bs;
    struct value *v;
    struct target_location_ctxt *tlctxt;

    if (dump_debug)
        fprintf(stdout,"INFO: Saving the state of the initial system call table.\n");
    /* Load the syscall table */
    bs = target_lookup_sym(target,"sys_call_table",NULL,NULL,
                                    SYMBOL_TYPE_FLAG_VAR);
    if (!bs) {
        fprintf(stderr, "ERROR: Could not lookup symbol sys_call_table!\n");
        return 1;
    }

    tlctxt = target_location_ctxt_create_from_bsymbol(target, TID_GLOBAL,bs);
    v = target_load_symbol(target,tlctxt,bs,LOAD_FLAG_NONE);
    if (!v) {
        fprintf(stderr,"ERROR: Could not load sys_call_table!\n");
        target_location_ctxt_free(tlctxt);
        bsymbol_release(bs);
        return 1;
    }
    syscall_table_vm = value_addr(v);
    if (dump_debug)
        fprintf(stdout,"INFO: Symbol syscall_table is at address %lx\n",
                                                        syscall_table_vm);
    value_free(v);
    target_location_ctxt_free(tlctxt);
    bsymbol_release(bs);

    if (dump_debug)
        fprintf(stdout,"INFO: Loading the syscall table.\n");
    if(target_os_syscall_table_load(target)) {
        fprintf(stderr,"ERROR: Failed to load the syscall table.\n");
        return 1;
    }

    max_num = target_os_syscall_table_get_max_num(target);
    if(max_num < 0) {
        fprintf(stderr,"ERROR: Failed to get the max number of target sysscalls.\n");
        return 1;
    }
    if (dump_debug)
        fprintf(stdout,"INFO: maximum number of system calls %d \n",max_num);

    /* Allocate memory for the sys_call_table and sys_call_names */
    sys_call_table  = calloc(max_num, sizeof(unsigned long));
    if(sys_call_table == NULL) {
        fprintf(stderr,"ERROR: Failed to allocate memory for sys_call_table.\n");
        return 1;
    }
    sys_call_names = calloc(max_num, sizeof(char *));
    if(sys_call_names == NULL) {
        fprintf(stderr,"ERROR: Failed to allocate memmory for sys_call_names.\n");
        free(sys_call_table);

        return 1;
    }

    function_prologue = calloc(max_num, sizeof(unsigned long *));
    if(function_prologue == NULL) {
        fprintf(stderr,"ERROR: Failed to allocate memory for function prologue.\n");
        free(sys_call_names);
        free(sys_call_table);
        return 1;
    }

    for(i = 0; i < max_num; i++) {
        sc = target_os_syscall_lookup_num(target, i);
        if(!sc) {
            continue;
        }
        if(sc->bsymbol) {

            if (dump_debug)
                fprintf(stdout,"%d\t %"PRIxADDR"\t%s\n", sc->num, sc->addr,
                                            bsymbol_get_name(sc->bsymbol));
            sys_call_table[sc->num] = sc->addr;
            sys_call_names[sc->num] = malloc(100);
            if(sys_call_names[sc->num] == NULL) {
                fprintf(stderr,"ERROR: Failed to allocate memory for the string.\n");
                goto fail;
            }
            strncpy(sys_call_names[sc->num], bsymbol_get_name(sc->bsymbol), 100);
            function_prologue[sc->num] = malloc(2 * sizeof(unsigned long));
            if (function_prologue[sc->num] == NULL) {
                fprintf(stderr,"ERROR: Failed to allocate memory for function prologue.\n");
                goto fail;
            }

            res = target_read_addr(target, sc->addr, 16, prologue);
            if(!res) {
                fprintf(stderr, "ERROR: Could not read 8 bytes at 0x%"PRIxADDR"!\n",sc->addr);
                goto fail;
            }
            memcpy(function_prologue[sc->num], &prologue,16);

        }
    }
    return 0;

 fail:
    for (i = 0; i < max_num; i++) {
        if (function_prologue[i])
            free(function_prologue[i]);
        if (sys_call_names[i])
            free(sys_call_names[i]);
    }
    free(function_prologue);
    free(sys_call_names);
    free(sys_call_table);
    return 1;
}

/* We are interested in monitoring certain programs, not all the programs.
 * programs_to_monitor[] : lists all the programs we are interested in
 */
bool
is_monitored_program(char *program)
{
    int	    i, n;

    n = sizeof(programs_to_monitor)/(sizeof(programs_to_monitor[0]));
    for(i=0; i<n; i++)
    {
	if(strstr(programs_to_monitor[i], program))
	    return true;
    }
    return false;
}



int gather_object_info(struct target *target, struct value *value, void * data) {

    struct value *pid_value;
    struct value *comm_value;
    struct value *mm_value;
    struct value *vm_area_value;
    struct value *file_value;
    struct value *path_value;
    struct value *dentry_value;
    struct value *d_name_value;
    struct value *len_name_value;
    struct value *file_name_value;
    struct value *next_vm_area_value;
    FILE * fp;
    ADDR next_vm_area_addr, file_value_addr;
    char *file_name, *process_name;
    int pid;
    char prev_name[50];
    int	    dump_debug = 0;

    fprintf(stdout, "INFO : Gathering shared object info ...\n");

    pid_value = target_load_value_member(target, NULL, value, "pid", NULL, LOAD_FLAG_NONE);
    if(!pid_value) {
        fprintf(stdout,"ERROR: Failed to load the pid value.\n");
        return 1;
    }
    pid = v_i32(pid_value);
    VALUE_FREE(pid_value);

    comm_value = target_load_value_member(target, NULL, value, "comm", NULL, LOAD_FLAG_NONE);
    if(!comm_value) {
        fprintf(stdout,"ERROR: Failed to load the process name.\n");
        return 1;
    }
    process_name = strdup(comm_value->buf);
    VALUE_FREE(comm_value);

    /* first check if the pointer to the mm struct is NULL*/
    mm_value = target_load_value_member(target, NULL, value, "mm", NULL,
                                        LOAD_FLAG_NONE);
    if (!mm_value || !(v_addr(mm_value))) {
        //fprintf(stdout, "INFO: Pointer to the mm struct is NULL \n");
        if (mm_value)
            VALUE_FREE(mm_value);
        return 0;
    }
    VALUE_FREE(mm_value);

    mm_value = target_load_value_member(target, NULL, value, "mm", NULL,
                                        LOAD_FLAG_AUTO_DEREF);
    if(!mm_value) {
        fprintf(stdout,"INFO: mm member is NULL.\n");
        return 0;
    }

    vm_area_value = target_load_value_member(target, NULL, mm_value, "mmap",
                                             NULL, LOAD_FLAG_AUTO_DEREF);
    if(!vm_area_value) {
        fprintf(stdout,"ERROR: Failed to load the mmap member value. \n");
        VALUE_FREE(mm_value);
        return 1;
    }

    fp = fopen(base_fact_file, "a+");
    if(fp == NULL) {
	int err = errno;
        fprintf(stdout," ERROR: Failed to open the base fact file %s\n", base_fact_file);
	fprintf(stdout," ERROR: %s\n", strerror(err));
        VALUE_FREE(vm_area_value);
        VALUE_FREE(mm_value);
        return 1;
    }

    if(is_monitored_program(process_name) == false)
    {
	printf("Not monitoring %s\n", process_name);
	fclose(fp);

	// We are interested only in certain programs, not all the programs
	free(process_name);
	return 0;
    }

    fprintf(fp,"( loaded-objects \n    \
		\t( comm \"%s\")\n \
    	        \t( pid %d)\n      \
	        \t( objects ",process_name, pid);
    free(process_name);

    /* Traverse through the entire list of vm_area
     * struct to get the name of loaded oobjects.
     */
    prev_name[0] = '\0';
    while(1) {
        unsigned int len;

        /* Firct check if the pointer to the vm_file struct is NULL */
        file_value = target_load_value_member(target, NULL, vm_area_value, "vm_file",
                                              NULL, LOAD_FLAG_NONE);
        if (!file_value || !(file_value_addr = v_addr(file_value))) {
            //fprintf(stdout,"INFO: vm_file value is null, so continuing . . \n");
            if (file_value)
                VALUE_FREE(file_value);
            goto nextptr;
        }
        VALUE_FREE(file_value);

        file_value = target_load_value_member(target, NULL, vm_area_value, "vm_file",
                                              NULL, LOAD_FLAG_AUTO_DEREF);

        /* Load the path the variable from the files struct*/
        if (dump_debug)
            fprintf(stdout,"INFO: Loading f_path struct\n");
        path_value = target_load_value_member( target, NULL, file_value, "f_path",
                    NULL, LOAD_FLAG_NONE);
        if(!path_value) {
            fprintf(stdout," ERROR: failed to load the path struct member.\n");
            VALUE_FREE(file_value);
            fclose(fp);
            VALUE_FREE(vm_area_value);
            VALUE_FREE(mm_value);
            return 1;
        }
        if (dump_debug)
            fprintf(stdout,"INFO: Loading dentry struct\n");
        dentry_value = target_load_value_member(target, NULL, path_value, "dentry",
                    NULL, LOAD_FLAG_AUTO_DEREF);
        if(!dentry_value){
            VALUE_FREE(path_value);
            VALUE_FREE(file_value);
            fprintf(stdout,"INFO: dentry member is NULL\n");
            goto nextptr;
        }

        /* Load the d_name struct */
        if (dump_debug)
            fprintf(stdout,"INFO: Loading d_name struct\n");
        d_name_value = target_load_value_member(target, NULL, dentry_value, "d_name",
                    NULL, LOAD_FLAG_NONE);
        if(!d_name_value) {
            fprintf(stdout," ERROR: failed to load the d_name struct member.\n");
            VALUE_FREE(path_value);
            VALUE_FREE(file_value);
            fclose(fp);
            VALUE_FREE(vm_area_value);
            VALUE_FREE(mm_value);
            return 1;
        }

        /* Finally load the length of  name string */
        if (dump_debug)
            fprintf(stdout,"INFO: Loading the length of name string\n");
        len_name_value = target_load_value_member( target, NULL, d_name_value, "len",
                                                   NULL, LOAD_FLAG_NONE);
        if(!len_name_value) {
            fprintf(stdout," ERROR: failed to load the name string.\n");
            VALUE_FREE(d_name_value);
            VALUE_FREE(path_value);
            VALUE_FREE(file_value);
            fclose(fp);
            VALUE_FREE(vm_area_value);
            VALUE_FREE(mm_value);
            return 1;
        }
        len = v_u32(len_name_value);
        VALUE_FREE(len_name_value);
        if (dump_debug)
            fprintf(stdout,"INFO: Length of the name string is %u \n.",len);
        if(len == 0) {
            if (dump_debug)
                fprintf(stdout,"INFO: File name length is 0 hence continuing with the loop\n");
            VALUE_FREE(d_name_value);
            VALUE_FREE(dentry_value);
            VALUE_FREE(path_value);
            VALUE_FREE(file_value);
            goto nextptr;
        }

        file_name_value = target_load_value_member(target, NULL, d_name_value, "name",
                    NULL, LOAD_FLAG_AUTO_STRING);
        if(!file_name_value) {
            fprintf(stdout,"ERROR: Could not load name of the file\n");
            VALUE_FREE(d_name_value);
            VALUE_FREE(dentry_value);
            VALUE_FREE(path_value);
            VALUE_FREE(file_value);
            goto nextptr;
        }

        file_name = strdup(file_name_value->buf);
        VALUE_FREE(file_name_value);
        if(strcmp(file_name, prev_name)) {
            if (dump_debug)
                fprintf(stdout,"INFO: Loaded object name %s\n",file_name);
            fprintf(fp," \"%s\" ",file_name);
        }
        strcpy(prev_name,file_name);
        free(file_name);

        VALUE_FREE(d_name_value);
        VALUE_FREE(dentry_value);
        VALUE_FREE(path_value);
        VALUE_FREE(file_value);

nextptr:

        /* first check if the vm_next pointer is null */
        next_vm_area_value = target_load_value_member(target, NULL, vm_area_value,
                                                "vm_next", NULL, LOAD_FLAG_NONE);
        if(!next_vm_area_value) {
            fprintf(stdout,"ERROR: Failed to load the next_vm_area_value.\n");
            fclose(fp);
            VALUE_FREE(vm_area_value);
            VALUE_FREE(mm_value);
            return 1;
        }

        next_vm_area_addr = v_addr(next_vm_area_value);
        if(!next_vm_area_addr) {
            if (dump_debug)
                fprintf(stdout,"INFO: Reached the end of the linked list.\n");
            VALUE_FREE(next_vm_area_value);
            VALUE_FREE(vm_area_value);
            break;
        }
        VALUE_FREE(next_vm_area_value);

        next_vm_area_value = target_load_value_member(target, NULL, vm_area_value,
                                                      "vm_next", NULL, LOAD_FLAG_AUTO_DEREF);
        VALUE_FREE(vm_area_value);
        vm_area_value = next_vm_area_value;
    }
    fprintf(fp," ))\n");
    fclose(fp);

    VALUE_FREE(mm_value);
    return 0;
}

int object_info(struct target *target) {

    int			ret_val;
    struct bsymbol	*init_task_bsymbol;
    int			dump_debug = 1;

    if(dump_debug)
        fprintf(stdout, "INFO: Gathering information about the loaded objects \n");

    generate_timestamp(base_fact_file);
    printf("Base File  = %s\n", base_fact_file);

    init_task_bsymbol = target_lookup_sym(target, "init_task", NULL, NULL,
            SYMBOL_TYPE_FLAG_VAR);
    if(!init_task_bsymbol) {
        fprintf(stdout,"ERROR: Could not lookup the init_task_symbol\n");
        return 1;
    }

    ret_val = os_linux_list_for_each_struct(target, init_task_bsymbol, "tasks", 0,
            gather_object_info, NULL);
    bsymbol_release(init_task_bsymbol);

    return ret_val;
}
int
gather_file_info(
	struct target	*target,
	struct value	*value,
	void		*data)
{
    struct value    *files_value    =	NULL;
    struct value    *fdt_value	    =	NULL;
    struct value    *max_fds_value  =	NULL;
    struct value    *fd_value;
    struct value    *file_value	    =	NULL;
    struct value    *path_value	    =	NULL;
    struct value    *dentry_value   =	NULL;
    struct value    *name_value;
    struct value    *d_name_value   =	NULL;
    struct value    *len_name_value;
    struct value    *file_name_value;
    struct value    *pid_value;
    struct value    *d_inode_value;
    struct value    *i_mode_value;
    struct bsymbol  *file_struct_bsymbol = NULL;
    int		    max_fds, i, pid;
    char	    *file_name	    =	NULL;
    char	    *process_name   =	NULL;
    ADDR	    file_addr, mem_addr;
    struct symbol   *file_struct_type;
    unsigned short  i_mode;
    FILE	    *fp		    =	NULL;
    int		    lnk		    =	0;
    int		    reg		    =	0;
    int		    dir		    =	0;
    int		    chr		    =	0;
    int		    blk		    =	0;
    int		    fifo	    =	0;
    int		    sock	    =	0;
    char	    lnk_file[64][100];
    char	    reg_file[64][100];
    char	    dir_file[64][100];
    char	    chr_file[64][100];
    char	    blk_file[64][100];
    char	    fifo_file[64][100];
    char	    sock_file[64][100];
    int		    dump_debug = 0;

    fprintf(stdout,"INFO: Gathering list of open files\n");

    pid_value = target_load_value_member(target, NULL, value, "pid", NULL, LOAD_FLAG_NONE);
    if(!pid_value)
    {
	fprintf(stdout," ERROR: failed to load the pid value.\n");
	return 1;
    }

    pid = v_i32(pid_value);
    VALUE_FREE(pid_value);
    name_value = target_load_value_member(target, NULL, value, "comm", NULL, LOAD_FLAG_NONE);
    if(!name_value)
    {
	fprintf(stdout," ERROR: failed to load the process name for pid %d.\n", pid);
	return 1;
    }

    process_name = strdup(name_value->buf);
    VALUE_FREE(name_value);
    fprintf(stdout,"INFO: Loading open file info for pid %d (%s).\n", pid, process_name);

    /* Load the files struct from the task_struct */
    if (dump_debug)
        fprintf(stdout,"INFO: Loading files struct\n");
    files_value = target_load_value_member(target, NULL, value, "files", NULL,
                                           LOAD_FLAG_NONE);
    if (!files_value) 
    {
        fprintf(stdout," ERROR: failed to load the files struct member.\n");
        goto fail;
    }
    if (!v_addr(files_value)) 
    {
        /* NULL address indicates a zombie/dead process */
        max_fds = 0;
    } 
    else 
    {
        VALUE_FREE(files_value);
        files_value = target_load_value_member(target, NULL, value, "files", NULL,
                                               LOAD_FLAG_AUTO_DEREF);
        if(!files_value) 
	{
            fprintf(stdout," ERROR: failed to load the files struct member.\n");
            goto fail;
        }

        /* Load the fdtable struct */
        if (dump_debug)
            fprintf(stdout,"INFO: Loading fdt struct\n");
        fdt_value =  target_load_value_member( target, NULL, files_value, "fdt",
                                               NULL, LOAD_FLAG_AUTO_DEREF);
        if(!fdt_value) 
	{
            fprintf(stdout," ERROR: failed to load the fdt struct member.\n");
            goto fail;
        }

        /* Load the  max_fds member of the ftable struct */
        if (dump_debug)
            fprintf(stdout,"INFO: Loading max_fds member\n");
        max_fds_value = target_load_value_member( target, NULL, fdt_value,
                                                  "max_fds", NULL, LOAD_FLAG_NONE);

        if(!max_fds_value) 
	{
            fprintf(stdout," ERROR: failed to load the max_fds member.\n");
            goto fail;
        }
        max_fds = v_i32(max_fds_value);
        VALUE_FREE(max_fds_value);
    }
    if (dump_debug)
        fprintf(stdout,"INFO: max_fds_value for process %s = %d\n", process_name, max_fds);

    /*Open the base fact file */
    if (dump_debug)
        fprintf(stdout,"INFO: Opening base fact file: %s\n",base_fact_file);
    
    fp = fopen(base_fact_file, "a+");
    if(fp == NULL) 
    {
        fprintf(stdout," ERROR: Failed to open the base fact file\n");
        goto fail;
    }

    /* Start encoding the fact */
    fprintf(fp,"\n(opened-files\n \
            \t(comm \"%s\")\n \
            \t(pid %d)\n", process_name, pid);

    free(process_name);
    process_name = NULL;

    for( i = 0; i < max_fds; i++) 
    {
        unsigned int len;

        if (dump_debug)
            fprintf(stdout,"INFO: Loading fd struct\n");
        fd_value =  target_load_value_member(target, NULL, fdt_value, "fd", NULL,
                                             LOAD_FLAG_NONE);
        if(!fd_value) 
	{
            fprintf(stdout," ERROR: failed to load the fd struct memeber.\n");
            goto fail;
        }

        /* Load the array of file descriptors */
        if (dump_debug)
            fprintf(stdout,"INFO: Loading fs struct\n");
        
	mem_addr = v_addr(fd_value);
        VALUE_FREE(fd_value);
        mem_addr = mem_addr + (target->arch->wordsize * i);
        if(!target_read_addr(target, mem_addr, target->arch->wordsize,
                        (unsigned char *)&file_addr)) 
	{
            fprintf(stdout,"ERROR: target_read_addr failed.\n");
            goto fail;
        }
        if(!file_addr) 
	{
            if (dump_debug)
                fprintf(stdout," INFO: File table entry is NULL\n");
            continue;
        }

        /* Load the type of symbol */
        file_struct_bsymbol = target_lookup_sym(target, "struct file", NULL,
                                                NULL, SYMBOL_TYPE_FLAG_TYPE);
        if(!file_struct_bsymbol) 
	{
            fprintf(stdout,"ERROR: Failed to lookup the struct file bsymbol.\n");
            goto fail;
        }

        file_struct_type = bsymbol_get_symbol(file_struct_bsymbol);
        if(!file_struct_type) 
	{
            fprintf(stdout,"INFO: Could not load the file struct type\n");
            goto fail;
        }

        /* Finally load the array memeber */
        if (dump_debug)
            fprintf(stdout,"INFO: Loading file struct\n");
        file_value = target_load_type(target, file_struct_type, file_addr,
                                      LOAD_FLAG_AUTO_DEREF);
        if(!file_value) 
	{
            fprintf(stdout," ERROR: failed to load the file struct member.\n");
            goto fail;
        }
        /* Load the path the variable from the files struct*/
        if (dump_debug)
            fprintf(stdout,"INFO: Loading f_path struct\n");
        path_value = target_load_value_member( target, NULL, file_value, "f_path",
                                               NULL, LOAD_FLAG_NONE);
        if(!path_value) 
	{
            fprintf(stdout," ERROR: failed to load the path struct member.\n");
            goto fail;
        }

        /* Load the dentry struct  member from the path */
        if (dump_debug)
            fprintf(stdout,"INFO: Loading dentry struct\n");
        dentry_value = target_load_value_member(target, NULL, path_value, "dentry",
                    NULL, LOAD_FLAG_AUTO_DEREF);
        if(!dentry_value)
	{
            VALUE_FREE(path_value);
            VALUE_FREE(file_value);
            bsymbol_release(file_struct_bsymbol);
            fprintf(stdout,"INFO: dentry member is NULL\n");
            continue;
        }

        /* Load the d_name struct */
        if (dump_debug)
            fprintf(stdout,"INFO: Loading d_name struct\n");
        d_name_value = target_load_value_member(target, NULL, dentry_value, "d_name",
                    NULL, LOAD_FLAG_NONE);
        if(!d_name_value) 
	{
            fprintf(stdout," ERROR: failed to load the d_name struct member.\n");
            goto fail;
        }
        /* Finally load the length of  name string */
        if (dump_debug)
            fprintf(stdout,"INFO: Loading the length of name string\n");
        len_name_value = target_load_value_member( target, NULL, d_name_value, "len",
                    NULL, LOAD_FLAG_NONE);
        if(!len_name_value) 
	{
            fprintf(stdout," ERROR: failed to load the name string.\n");
            goto fail;
        }
        len = v_u32(len_name_value);
        VALUE_FREE(len_name_value);
        if (dump_debug)
            fprintf(stdout,"INFO: Length of the name string is %u \n.",len);
       
       	if(len == 0) 
	{
            if (dump_debug)
                fprintf(stdout,"INFO: File name length is 0 hence continuing with the loop\n");
            VALUE_FREE(d_name_value);
            VALUE_FREE(dentry_value);
            VALUE_FREE(path_value);
            VALUE_FREE(file_value);
            bsymbol_release(file_struct_bsymbol);
            continue;
        }

        file_name_value = target_load_value_member(target, NULL, d_name_value, "name",
                    NULL, LOAD_FLAG_AUTO_STRING);
        if(!file_name_value) 
	{
            fprintf(stdout,"ERROR: Could not load name of the file\n");
            VALUE_FREE(d_name_value);
            VALUE_FREE(dentry_value);
            VALUE_FREE(path_value);
            VALUE_FREE(file_value);
            bsymbol_release(file_struct_bsymbol);
            continue;
        }

        file_name = strdup(file_name_value->buf);
        VALUE_FREE(file_name_value);
        VALUE_FREE(d_name_value);
        /* Load the inode struct */
        if (dump_debug)
            fprintf(stdout,"INFO: Loading the d_inode struct.\n");
        d_inode_value = target_load_value_member(target, NULL, dentry_value, "d_inode",
                                                 NULL, LOAD_FLAG_AUTO_DEREF);
        if(!d_inode_value) 
	{
            fprintf(stdout,"ERROR: failed to load the d_inode member.\n");
            goto fail;
        }

        /*Load the i_mode member */
        if (dump_debug)
            fprintf(stdout,"INFO: Load the i_mode member.\n");
        i_mode_value = target_load_value_member(target, NULL, d_inode_value, "i_mode",
                                                NULL, LOAD_FLAG_NONE);
        if(!i_mode_value) 
	{
            fprintf(stdout,"ERROR: failed to load the i_mode value.\n");
            VALUE_FREE(d_inode_value);
            goto fail;
        }
        i_mode = v_u16(i_mode_value);
        VALUE_FREE(i_mode_value);
        VALUE_FREE(d_inode_value);

        VALUE_FREE(dentry_value);
        VALUE_FREE(path_value);
        VALUE_FREE(file_value);
        bsymbol_release(file_struct_bsymbol);
        file_struct_bsymbol = NULL;

        /* Now check for the type of the file*/
        if(S_ISLNK(i_mode)) {
            strcpy(lnk_file[lnk++], file_name);
        }
        else if(S_ISREG(i_mode)) {
            strcpy(reg_file[reg++], file_name);
        }
        else if(S_ISDIR(i_mode)){
            strcpy(dir_file[dir++], file_name);
        }
        else if(S_ISCHR(i_mode)) {
            strcpy(chr_file[chr++], file_name);
        }
        else if(S_ISFIFO(i_mode)){
            strcpy(fifo_file[fifo++], file_name);
        }
        else if(S_ISSOCK(i_mode)) {
            strcpy(sock_file[sock++], file_name);
        }
        else if(S_ISBLK(i_mode)) {
            strcpy(blk_file[blk++], file_name);
        }
        else {
        }

        free(file_name);
        file_name = NULL;
    }

    if (fdt_value)
        VALUE_FREE(fdt_value);
    if (files_value)
        VALUE_FREE(files_value);

    /* Write this infomation into the file as base facts  */
    int c = 0;
    if(lnk) 
    {
        fprintf(fp,"\t (lnk_count %d) \n \
                    \t (lnk_files ",(lnk));
        for(c = 0; c < lnk; c++) {
            fprintf(fp," \"%s\" ", lnk_file[c]);
        }
        fprintf(fp," )\n");
    }
    if(reg) 
    {
        fprintf(fp,"\t (reg_count %d) \n \
               \t (reg_files ",(reg));
        for(c = 0; c < reg; c++) {
            fprintf(fp," \"%s\" ", reg_file[c]);
        }
        fprintf(fp," )\n");
    }

    if(dir) 
    {
        fprintf(fp,"\t (dir_count %d) \n \
               \t (dir_files ",(dir));
        for(c = 0; c < dir; c++) {
            fprintf(fp," \"%s\" ", dir_file[c]);
        }
         fprintf(fp," )\n");
    }

    if(chr) 
    {
        fprintf(fp,"\t (chr_count %d) \n \
               \t (chr_files ",(chr));
        for(c = 0; c < chr; c++) {
            fprintf(fp," \"%s\" ", chr_file[c]);
        }
        fprintf(fp," )\n");
    }
    
    if(blk) 
    {
        fprintf(fp,"\t (blk_count %d) \n \
               \t (blk_files ",(blk));
        for(c = 0; c < blk; c++) {
            fprintf(fp," \"%s\" ", blk_file[c]);
        }
        fprintf(fp," )\n");
    }

    if(fifo) 
    {
        fprintf(fp,"\t (fifo_count %d) \n \
               \t (fifo_files ",(fifo));
        for(c = 0; c < fifo; c++) {
            fprintf(fp," \"%s\" ", fifo_file[c]);
        }
        fprintf(fp," )\n");
    }

    if(sock) 
    {
        fprintf(fp,"\t (sock_count %d) \n \
               \t (sock_files ",(sock));
        for(c = 0; c < sock; c++) {
            fprintf(fp," \"%s\" ", sock_file[c]);
        }
        fprintf(fp," )\n");
    }

    fprintf(fp,"\t (num_opened_files %d ))\n",(lnk + reg + dir
                + chr + blk + fifo + sock ));
    fclose(fp);

    return(0);
 fail:
    if (d_name_value)
        VALUE_FREE(d_name_value);
    if (dentry_value)
        VALUE_FREE(dentry_value);
    if (path_value)
        VALUE_FREE(path_value);
    if (file_value)
        VALUE_FREE(file_value);
    if (file_struct_bsymbol)
        bsymbol_release(file_struct_bsymbol);
    if (fp)
        fclose(fp);
    if (fdt_value)
        VALUE_FREE(fdt_value);
    if (files_value)
        VALUE_FREE(files_value);
    if (process_name)
        free(process_name);

    return 1;
}



int gather_module_info(
	struct target	*target, 
	struct value	*value, 
	void		*data)
{
    struct value    *name_value;
    char	    *module_name;
    FILE	    *fp = NULL;


    name_value = target_load_value_member(target, NULL, value, "name", NULL, LOAD_FLAG_NONE);
    if(!name_value) {
        fprintf(stdout," ERROR: failed to load the process name.\n");
        return 1;
    }

    module_name = strdup(name_value->buf);
    VALUE_FREE(name_value);
    fprintf(stdout,"INFO: Module name: %s.\n",module_name);

    fp = fopen(base_fact_file, "a+");
    if(fp == NULL) {
        fprintf(stdout," ERROR: Failed to open the base fact file\n");
        free(module_name);
        return 1;
    }

    /* Start encoding the fact */
    fprintf(fp,"\n(loaded-module\n \
            \t(name  \"%s\"))\n",module_name);

    fclose(fp);
    free(module_name);
    return 0;
}


int 
get_file_info(struct target *target) 
{
    int		    ret_val;
    struct bsymbol  *init_task_bsymbol;

    generate_timestamp(base_fact_file);
    init_task_bsymbol = target_lookup_sym(target, "init_task", NULL, NULL,
					  SYMBOL_TYPE_FLAG_VAR);
    if(!init_task_bsymbol) 
    {
        fprintf(stdout,"ERROR: Could not lookup the init_task_symbol\n");
        return 1;
    }

    ret_val = os_linux_list_for_each_struct(target, init_task_bsymbol, "tasks", 0,
					    gather_file_info, NULL);
    bsymbol_release(init_task_bsymbol);
    return ret_val;
}



int gather_commandline_info(struct target *target, struct value *value, void * data) {

    char                        targetstr[80];
    struct target		*overlay_target = NULL;
    struct target_spec          *overlay_spec   =   NULL;
    struct value *pid_value;
    int pid;
    struct value *comm_value;
    struct value *mm_value;
    struct value *arg_start_value;
    unsigned long arg_start;
    struct value *arg_end_value;
    unsigned long arg_end;
    unsigned long length = 0;
    struct value *env_start_value;
    unsigned long env_start;
    struct value *env_end_value;
    unsigned long env_end;
    char	    *process_name = NULL;

    ADDR paddr;
    unsigned char *command_line, *ret, *environment;
    FILE *fp;

    pid_value = target_load_value_member(target, NULL, value, "pid", NULL, LOAD_FLAG_NONE);
    if(!pid_value) {
        fprintf(stdout,"ERROR: Failed to load the pid value.\n");
        return 1;
    }
    pid = v_i32(pid_value);
    VALUE_FREE(pid_value);

    comm_value = target_load_value_member(target, NULL, value, "comm", NULL, LOAD_FLAG_NONE);
    if(!comm_value) {
        fprintf(stdout,"ERROR: Failed to load the process name.\n");
        return 1;
    }
    process_name = strdup(comm_value->buf);
    VALUE_FREE(comm_value);

    if(is_monitored_program(process_name) == false)
    {
	printf("********** NOT INTERESTED In %s **************\n", process_name);
	free(process_name);
	return 0;
    }

    printf(" -------------------------- SSHD DISECTION[%d] ----------------------------\n", pid);
    /* Check if the mm strcuture is NULL */
    mm_value = target_load_value_member(target, NULL, value, "mm", NULL,
                                        LOAD_FLAG_NONE);
    if(!mm_value || !(v_addr(mm_value))) {
            fprintf(stdout, "INFO: Pointer to the mm struct is NULL \n");
        if (mm_value)
            VALUE_FREE(mm_value);
        return 0;
    }
    VALUE_FREE(mm_value);
    /* Lgad the mm member */
    mm_value = target_load_value_member(target, NULL, value,"mm", NULL, LOAD_FLAG_AUTO_DEREF);
    if(!mm_value) {
            fprintf(stdout,"INFO: Pointer to the mm struct is null.\n");
        return 0;
    }
    arg_start_value = target_load_value_member(target, NULL, mm_value, "arg_start",
                                                NULL, LOAD_FLAG_NONE);
    if(!arg_start_value) {
        fprintf(stdout,"ERROR: Failed to load the arg_start memeber.\n");
        VALUE_FREE(mm_value);
        return 1;
    }
    arg_start = v_u64(arg_start_value);
    VALUE_FREE(arg_start_value);

    arg_end_value = target_load_value_member(target, NULL, mm_value, "arg_end",
                                                NULL, LOAD_FLAG_NONE);
    if(!arg_end_value) {
        fprintf(stdout,"ERROR: Failed to load the arg_end memeber.\n");
        VALUE_FREE(mm_value);
        return 1;
    }
    arg_end = v_u64(arg_end_value);
    VALUE_FREE(arg_end_value);

    length = arg_end - arg_start;
    if(!length) {
        fprintf(stdout," INFO: No command line for the process with pid %d\n",pid);
    }

    /* Now convert the virtual address into physical address */
    if(target_addr_v2p(target,pid, arg_start, &paddr)) {
        fprintf(stdout,"ERROR: could not translate virtual address 0x%"PRIxADDR"\n", arg_start);
        VALUE_FREE(mm_value);
        return 1;
    }
        fprintf(stdout,"INFO: virtual address 0x%"PRIxADDR" translates to 0x%"PRIxADDR"\n",
                                                                arg_start,paddr);

   /* Now read the buffer contents from the physical address*/
    command_line = calloc(100+1, sizeof (char));

    ret = target_read_physaddr(target, paddr, 100, command_line);
    if(!ret) {
        fprintf(stdout,"ERROR: Failed to load the commandline buffer.\n");
        free(command_line);
        VALUE_FREE(mm_value);
        return 1;
    }


    /* Gather information reagarding the environment of the process. */
     env_start_value = target_load_value_member(target, NULL, mm_value, "env_start",
                                                NULL, LOAD_FLAG_NONE);
    if(!env_start_value) {
        fprintf(stdout,"ERROR: Failed to load the env_start memeber.\n");
        free(command_line);
        VALUE_FREE(mm_value);
        return 1;
    }
    env_start = v_u64(env_start_value);
    VALUE_FREE(env_start_value);

    env_end_value = target_load_value_member(target, NULL, mm_value, "env_end",
                                                NULL, LOAD_FLAG_NONE);
    if(!env_end_value) {
        fprintf(stdout,"ERROR: Failed to load the env_end memeber.\n");
        free(command_line);
        VALUE_FREE(mm_value);
        return 1;
    }
    env_end = v_u64(env_end_value);
    VALUE_FREE(env_end_value);
    VALUE_FREE(mm_value);

    if (env_end <= env_start) {
        fprintf(stdout," INFO: No command line for the process with pid %d\n",pid);
        length = 0;
    }
    length = env_end - env_start;

    /* Now convert the virtual address into physical address */
    if(target_addr_v2p(target,pid, env_start, &paddr)) {
        fprintf(stdout,"ERROR: could not translate virtual address 0x%"PRIxADDR"\n", env_start);
        free(command_line);
        return 1;
    }

    /* Now read the buffer contents from the physical address*/
    environment = calloc(100+1, sizeof (char));
    if (length > 100)
            length = 100;

    ret = target_read_physaddr(target, paddr, length, environment);
    if(!ret) {
        fprintf(stdout,"ERROR: Failed to load the environment buffer.\n");
        free(environment);
        free(command_line);
        return 1;
    }

    fp = fopen("/mnt/extra/logs/cmd_line_info.txt", "a+");
    if(fp == NULL) {
        fprintf(stdout," ERROR: Failed to open the base fact file\n");
        free(environment);
        free(command_line);
        return 1;
    }

    fprintf(fp,"( command_line \n    \
                \t( command \"%s\")\n \
                \t( environment \"%s\"))\n", command_line, environment);

    fclose(fp);
    
    overlay_target = target_lookup_overlay(target, pid);
    if(overlay_target == NULL)
    {
        overlay_spec = target_build_default_overlay_spec(target, pid);
        if(overlay_spec == NULL)
        {
            fprintf(stderr, "ERROR: target_build_default_overlay_spec failed.\n");
            exit(-1);
        }

        overlay_spec->debugfile_root_prefix = strdup(DEBUG_FILE_ROOT_PREFIX);
        overlay_spec->target_type = TARGET_TYPE_OS_PROCESS;
        overlay_spec->style = PROBEPOINT_SW;
        overlay_target = target_instantiate_overlay(target, pid, overlay_spec);
        if(overlay_target == NULL)
        {
            fprintf(stderr, "target_instantiate_overlay failed!\n");
            exit(-1);
        }
        else
           printf("***** successfully instantiated overlay target ****\n");

        if(target_open(overlay_target))
        {
            fprintf(stderr, "SHO_ERROR : could not open %s!\n", targetstr);
            exit(-1);
        }

    }

    version_scanner(overlay_target, target);

    free(environment);
    free(command_line);
    return 0;
}


int commandline_info(struct target *target) {

    int ret_val;
    struct bsymbol * init_task_bsymbol;

    init_task_bsymbol = target_lookup_sym(target, "init_task", NULL, NULL,
            SYMBOL_TYPE_FLAG_VAR);
    if(!init_task_bsymbol) {
        fprintf(stdout,"ERROR: Could not lookup the init_task_symbol\n");
        return 1;
    }
    ret_val = os_linux_list_for_each_struct(target, init_task_bsymbol, "tasks", 0,
            gather_commandline_info, NULL);
    return ret_val;
}

int generate_snapshot(struct target *target) {
    int result = 0;
    target_status_t status;


    /* Pause the target */
    if ((status = target_status(target)) != TSTATUS_PAUSED) {
        if (target_pause(target)) {
                fprintf(stderr,"ERROR: Failed to pause the target \n");
                result = 1;
                goto resume;
         }
    }

    result = commandline_info(target);
    if( result) {
        fprintf(stderr,"ERROR: commandline_info failed.\n");
        goto resume;
    }

resume:
    if ((status = target_status(target)) == TSTATUS_PAUSED) {
        if (target_resume(target)) {
            fprintf(stderr, "ERROR: Failed to resume target.\n ");
            result = 1;
        }
    }

    return result;
}
