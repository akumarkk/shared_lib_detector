\documentclass{sig-alternate-05-2015}
\newcommand{\MyCode}[1]{%
\texttt{#1}}

\usepackage{csquotes}
\usepackage{multirow}
\usepackage{afterpage}
\usepackage{placeins}
\usepackage[inline]{enumitem}
\usepackage[table,x11names]{xcolor}
\usepackage{siunitx}
%\usepackage[none]{hyphenat}

\begin{document}



\title{ VmiCVS: Cloud Vulnerability Scanner}



\numberofauthors{2}  
\author{
\alignauthor
Anil Kumar Konasale Krishna\titlenote{}\\
       \affaddr{University of Utah}\\
       \affaddr{Salt Lake City, USA}\\
       \email{akumarkk@cs.utah.edu}  
% 2nd. author
\alignauthor
Robert Ricci\titlenote{}\\
       \affaddr{University of Utah}\\
       %\affaddr{P.O. Box 1212}\\
       \affaddr{Salt Lake City, USA}\\
       \email{ricci@cs.utah.edu}
}

\maketitle
\begin{abstract}

Every service that runs in cloud systems comes with its own set of vulnerabilities. It is important to detect and assess those vulnerabilities to provide seamless and secure service to the users. Various scanners such as Port scanner, Network scanner, Web application security scanner, Database security scanner, Host based vulnerability scanner etc provide security assessment. But these scanners use methods that an attacker uses to attack in order to expose the vulnerabilities. As a result, application ecosystem might get disturbed and hard-to-attack vulnerabilities might left undetected. A yet another set of scanners check version of the service through protocol level messages in order to determine the vulnerabilities applicable to that particular service version. With this approach, certain vulnerabilities are not discovered when a particular software piece(example : glibc) is not directly exposed to the remote user.\par 

We propose a novel Cloud Vulnerability Scanner, VmiCVS (Virtual Machine Introspection based Cloud Vulnerability Scanner). It provides security assessment of vulnerabilities even if the software is hidden from remote user and without disturbing application ecosystem. It can be used by cloud provider to  provide Vulnerability scanning-as-a-service where detected vulnerabilities are reported to tenant for additional incentives. We have evaluated our scanner by assessing the vulnerabilities of services such as \emph{sshd} and hidden(from remote user) libraries such as \emph{glibc} and \emph{libcrypto}. 

\end{abstract}


\keywords{Virtual Machine Introspection; Vulnerability Scanner; Version Scanner}

\section{Introduction}
\label{introduction}

Every service that runs in cloud systems comes with its own set of vulnerabilities. It is important to detect and assess those vulnerabilities to provide seamless and secure service to the targeted users. The recent Distributed Denial of Service (DDoS) attack on American Banks, \enquote{zero-day} vulnerability ~\cite{facebook} discovered at Facebook, Buffer Overflow bug exploited by Code Red~\cite{codeRed}, Sasser~\cite{sasser} and Blaster~\cite{blaster}, OpenSSL Null pointer assignment flaw~\cite{openSSLDoS} exploited to cause Denial of Service(DoS) attack etc shows the importance and imminence of Cloud vulnerability Assessment tools. Intrusion Detection and Prevention Systems(IDS/IPS), Firewalls and Anti-Virus are used as security solutions. Attacker can still bypass these security solutions and exploit known vulnerabilities. As reported in 2015 Data Breach Investigations Report~\cite{dataBreachReport}, 99.9\% of the exploited vulnerabilities had been compromised more than a year after the associated CVE was published. As reported in Vulnerability Assessment report~\cite{vulnAssessmentWP}, one of the 70 largest security breaches were achieved through the exploitation of known vulnerability in spite of the presence of correctly installed Firewalls, IDS/IPS and Anti-virus. These reports indicates the need for vulnerability assessment systems even in presence of other security solutions. 

Existing Vulnerability Assessment (VA) Scanners do not produce accurate results. VA scanners produce many \emph{false negatives}.As stated in ~\cite{versionAndBehavior}, nearly all VA scanners rely on version checking as their primary method of known vulnerability assessment. Version number is extracted from the response header. But this method of extracting version number is not reliable as 
\begin{enumerate*}[label={\alph*)}]
\item Header does not contain enough information such as patch number
\item Application itself is configured to not reveal the version information in the header to make the attack harder
\item Firewall could manipulate the header information or header can be hidden and suppressed. 
\end{enumerate*}
 \emph{System library version information is not included in the protocol header fields}. So version analysis does not list the vulnerabilities in the system libraries. Though VA scanners perform Behavior Analysis, as the potential known vulnerability list is huge, it is not possible to assess all of them by exploitation tests. Further, evaluation of seven VA scanners performed in ~\cite{evaluation} shows that automated vulnerability scanning is not able to accurately identify all the vulnerabilities present in the system. Though Authenticated VA scanning produces better results, it requires system credentials.

Though there exist tens of Vulnerability Assessment Scanners in the Market, they still have to evolve. Most of the existing Vulnerability Assessment Scanners are not \emph{safe}. As reported in ~\cite{notSafe1} and ~\cite{notSafe2}, scanners can crash the system under testing, disrupt normal network operations etc. In some sense, this is not surprising as scanners use crafted network packets to exploit the vulnerability like an attacker. For example, in order to assess buffer overflow bug, scanners send enough data to overflow the buffer and the overflow could cause any unpredictable program execution including system crash.\par


%Current VA scanners are time consuming and consumes significant network and computing resources to list known vulnerabilities. 

We propose a novel Virtual Machine Introspection(VMI) based Cloud Vulnerability Assessment Scanner, \emph{VmiCVS}. It is \emph{safe} as it does not use crafted network protocol packets to exploit vulnerabilities in order to assess them. Instead, it produces accurate results by performing version analysis of all pieces of software including system libraries using VMI techniques. It does not need system credentials to produce accurate results. 

Most of the software libraries and services have their version number stored in a symbol in their binaries. These constant symbols are loaded into \emph{data segment} of the process memory layout. \emph{stackdb} is a VMI library, which provides APIs to read value of any particular symbol of a running process or a library linked to running instance of a binary. It also provides the most powerful and simpler APIs to read the virtual memory pages of a process. VmiCVS reads the virtual memory pages of a process through \emph{stackdb} APIs and runs a \emph{version parser} program to extract the version string. Through this method, even the version of libraries such as \emph{glibc}, \emph{libcrypto} etc can be determined.

We have evaluated VmiCVS by scanning the version of three different pieces of software; \emph{sshd},running instance of a process binary, \emph{libcrypto}, library that contains version number as a part of its name, \emph{glibc}, library that do not contain version number in its name.

The rest of the paper is as organized as follows: In Section \ref{introduction} we discuss Background. In Section \ref{architecture} we present our VmiCVS architecture, while the VmiCVS implementation is detailed in the Section \ref{implementation}. Evaluation of VmiCVS is explained in the Section \ref{evaluation} and finally, conclusion and further research directions are given in the Section \ref{conclusion}. 


\section{Background}
\label{background}

Cloud computing services such as Infrastructure as a Service(IaaS), Software as a Service(SaaS) and Platform as a Service(PaaS) are delivered through virtualized computing resources. Virtualization is the main enabling technology for cloud computing. Hardware Virtualization abstracts underlying physical hardware and allows multiple instances of operating systems to be running on the same physical machine with the help of Hypervisor. Hypervisor/Virtual Machine Monitor(VMM) is a software which provides virtual operating platform for guest operating systems and manages the execution of the guest operating systems. There are two types of VMM ~\cite{HypervisorWiki}: 
\begin{enumerate}
\item Type-1 :  It runs directly on the host's hardware to control the hardware and to manage guest operating systems. For Example, XEN
\item Type-2 : It runs on conventional host operating system just as another process. For example, KVM 
\end{enumerate}

It is very important to monitor the host in order to detect and report any malicious activity that compromises the host.\textbf{Virtual Machine Introspection (VMI)} is used to achieve this. VMI is a technique of introspecting Virtual Machine from outside of it for the purpose of analyzing the software running inside it. VMI is realized by interposing at hardware level which allows to mediate the interaction between hardware and the host software. 

\textbf{Stackdb} is a debugging library with VMI support, which allows one to control and monitor the whole system at multiple levels. It can be used to monitor guest operating system itself, process running in guest or language runtime. It directly interacts with the system being debugged through Hypervisor interface. Stackdb provides APIs to install break points, watch the values of symbols in the processes running inside guest, read values of symbols of a process etc. Stackdb's models process address space as follows : An address space is divided into number of regions and regions are further divided into number of ranges. Related ranges are grouped into regions. Basically a range is a continuous chunk of memory with uniform protection bits. For example, a shared library loaded into process address space may spread across more than two  ranges. All the ranges where a shared library is mapped becomes a region. In Linux, range corresponds to \texttt{vm\_area\_struct}. There could be more than one such structure associated with a memory mapped shared library.

 \textbf{Vulnerability Assessment} Vulnerability can be defined as a flaw or bug in the system that present the opportunity for malicious exploitation and results in security breach or a violation of system security policy.Vulnerability Assessment~\cite{vulnAssmtWiki} is a process of identifying, quantifying and prioritizing the vulnerabilities in the system.

\section{Architecture}
\label{architecture}

VmiCVS architecture is as shown in the Figure~\ref{fig:arch}
\begin{figure}
\centering
\includegraphics[width=9cm,height=22cm,keepaspectratio,trim=4cm 0cm 0.5cm 0cm,clip]{images/architecture.jpg}
\caption{Cloud Vulnerability Scanner Architecture}
\label{fig:arch}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=7.5cm,keepaspectratio,trim=0.5cm 3cm 0.5cm 3cm,clip]{images/vmiCVSProgram.jpg}
\caption{VmiCVS Version Scanner}
\label{fig:scanner}
\end{figure}

It consists of Machine hardware, Host OS, VmiCVS version scanner, Stackdb layer, Hypervisor and Guest OS. As shown in the Figure\ref{fig:arch}, Host Operating System(OS) runs directly on hardware. VmiCVS version scanner program runs in Host OS and uses stackdb provided API layer to interact with kernel and user processes running in Guest OS. As shown in the Figure \ref{fig:scanner}, it consists of \textbf{\emph{Version Scanner program}} and  \textbf{\emph{Version Parser program}}.

\begin{itemize}
\item \emph{Version Scanner Program} It extracts the version information of desired library/binary running in the guest OS using stackdb APIs and dump it into a File. Version number of a library can be extracted from either the name of the library or through the virtual memory pages of the library. Based on how the version information is extracted, there are two types of version scanners 
\begin{enumerate}
\item \emph{Virtual Memory based Version Scanner} It dumps the virtual memory page contents of a process into a file in order to extract the version number. Usually version number is stored in a symbol/macro in binary/library executable. For example, \emph{glic} version number is stored in a symbol, \texttt{banner}. So virtual memory pages pointing to the \emph{text segment} of a library/binary is the source of version number information in this case. It walks through the task list until it finds the desired process and dump all the virtual memory page contents into a file through stackdb's \emph{overlay target}.

\item \emph{Name based Version Scanner} It dumps all the shared library names linked to a particular process into a file. Some shared library names contain version number. So shared library name is the source of information in this case. To list all the shared libraries of a process, it attaches to the guest operating system through base driver and walks through the task list until it finds the one which carries the  version number of desired software piece and dump all its shared library names. 
\end{enumerate}

Both the Version Scanners include three step process to dump the version information :
\begin{itemize}
\item \emph{Initialization} Initializes stackdb and VMI environment.
\item \emph{Process Lookup} In this step, start of the task list of guest operating system is retrieved and lookup for the required process is performed by traversing the task list.
\item \emph{process overlay Handle Retrieval} stackdb overlay target handler corresponding to the desired process is retrieved in this step.
\item \emph{Information Extraction} Information such as virtual memory contents or shared library names is dumped into the file on a disk.
\end{itemize}

\item \emph{Version Parser Program} It parses the output file produced by the version scanner program, based on the version pattern and report the version number. For example, pattern to extract the version of \emph{glibc} is \emph{Ubuntu EGLIBC \%d.\%d-0ubuntu\%d.\%d}
\end{itemize}


\section{Design}
\label{design}

VmiCVS consists of two types of version scanners

\begin{itemize}
\item
\label{nameBasedVS}
\textbf{Name based Version Scanner} As described in the paper ~\cite{versioningUsenix}, virtually all the systems incorporate some form of version number in the filename of each library in order to indicate and manage the upward compatibility. GNU libraries follow 
\begin{center}
lib<name>.so.<major>.<minor>.<rls> 
\end{center}
format for the shared library names.For example libraries generated by Cyrus SASL package~\cite{libSasl} such as \emph{liblogin.so$.2.0.25$}, \emph{libplain.so$.2.0.25$}, \emph{libdigestmd5.so} \-\emph{$.2.0.25$}, \emph{libsasldb.so$.2.0.25$}, \emph{libsasl.so$.2.0.25$}, Linux PAM package~\cite{libPam} such as \emph{libpam\_misc.so$.0.82.1$}, \emph{libpam.so$.0.$} \-\emph{$84.1$}, \emph{libpamc.so$.0.82.1$} and CrackLib package~\cite{libCrack} libraries such as \emph{libcrack.so$.2.9.0$}. So version number of such libraries can be extracted by finding the file name of the library. Tools such as \emph{libtool} are available to automatically build libraries with version number in their file names.\par


Shared libraries are linked against process binaries. In a running process, each such shared library file is loaded as a memory mapped file and its information is stored in kernel memory map data structure. As shown in the Figures\ref{fig:struct1}, each process is represented by a kernel data structure, \texttt{task\_struct}. Information about memory mapped files is stored in \texttt{struct mm\_struct *mm} member of \texttt{task\_struct}. Each shared library linked against a process consists of text segment, data segment and bss. Each segment is mapped to a particular piece of process address space. This mapping information including shared library file name and other attributes are stored in \texttt{vm\_area\_struct} corresponding to data, text and bss segments of a library. \texttt{vm\_area\_s\-truct} structure associated with each segment is linked with \texttt{vm\_area\_struct} of another segment through doubly linked list. The head of the link list is stored in \texttt{mmap} member of \texttt{struct mm\_struct} shown in the Figure ~\ref{fig:struct1}. The file name of the shared library can be obtained by scanning all the linked structures as shown in the Figure \ref{fig:struct1}.

\begin{figure*}
\includegraphics[width=\linewidth,trim=0.0cm 14cm 0.5cm 0cm,clip]{images/vmAddressSpace.jpg}
\caption{Kernel data structures traversed to read the shared library file name}
\label{fig:struct1}
\end{figure*}

%\begin{figure}
%\includegraphics[scale=0.2]{images/vmFile.png}
%\caption{Expansion of \emph{struct file}}
%\label{fig:struct2}
%\end{figure}


\item \textbf{Virtual Memory based Version Scanner} process binaries and some libraries such as \emph{glibc} do not contain version number as a part of their file names. For such libraries/binaries, this method is applied to obtain the version number.

Most of the shared libraries and binaries have their version number stored in a symbol as a constant literal or stored as a part of banner string. For example, version number of \emph{glibc} is stored as a part of banner string in a variable \texttt{banner},  release version number of \emph{sshd} binary is coded in the macro \texttt{SSH\_VERSION}. These pre-defined version strings are stored in \emph{text segment} of the library and are loaded into data segment based on the platform and operating system. As these segments are mapped to virtual memory pages of the process, contents of the memory pages are dumped into a file by version scanner program using stackdb APIs. Later \emph{Version Parser} is used to extract the potential version string.
\end{itemize}
 

\section{Implementation}
\label{implementation}

Version string of a process binary or shared library is obtained by reading the virtual memory pages or by listing the shared library names. VmiCVS version scanner programs need to initialize the stackdb environment through stackdb APIs before probing or monitoring processes in guest operating system. VmiCVS program also provides command line options to specify the process or library name, whose version number needs to be scanned.
 
\begin{itemize}
\item \emph{--vscanner-log-file} specifies log file name where to dump virtual memory contents of a process.
\item \emph{--vscanner-olay-process} option to specify overlay process, of which version user interested in.
\item \emph{--vscanner-olay-library} option to specify library name whose version user is looking for.
\end{itemize}


Implementation of two types of Version Scanners is explained below. 

\begin{enumerate}
\item \emph{Name based version Scanner} It consists of three steps 
\begin{itemize}
\item \emph{Initialization} includes initializing stackdb and VMI environment and attaching to the target Guest OS.

\item \emph{Process Lookup} step retrieves head of guest OS's task list and traverses the list to find the \texttt{task\_st\-ruct} corresponding to the desired process. 

\item \emph{Information Extraction}
As explained in the design section, shared library files are loaded into process address space as memory mapped files. Hence memory map data structures are read to obtain the file name of a particular shared library. 

stackdb provides APIs to read the values of any structure member given structure and member name. For example, to read the value of \texttt{comm}, \texttt{task\_struct} structure and \emph{comm} as a query are passed to stackdb APIs. Initially \texttt{comm} member of \texttt{task\_struct} is read to check the process name. Next, as shown in the Figure\ref{fig:struct1}, values of \texttt{mm}, \texttt{mmap}, \texttt{vm\_file}, \texttt{f\_path}, \texttt{dentry} and \texttt{dname} symbols are read to list the name of memory mapped shared libraries.

\begin{figure*}
\includegraphics[width=\linewidth,keepaspectratio,trim=0.5cm 4cm 0cm 6cm,clip]{images/stackdbMemoryModel.jpg}
\caption{Stackdb Memory Model of a Process}
\label{fig:stackdbmm}
\end{figure*}

\end{itemize}

\item \emph{Virtual Memory based version Scanner}
\begin{itemize}
\item \emph{Initialization} includes initializing stackdb and VMI environment and attaching to the target Guest OS.

\item \emph{Process Lookup} step retrieves head of guest OS's task list and traverses the list to find the \texttt{task\_st\-ruct} corresponding to the desired process. 

\item \emph{Overlay handle retrieval} stackdb handle holding the meta data of the desired process needs to be retrieved from the base handle pointing to the guest OS using \emph{stackdb} API \texttt{os\_linux\_list\_for\-\_each\_struct()}. This API invokes a callback function supplied as argument to it on each structure.

\item \emph{Information Extraction}
Contents of the virtual memory pages of a process are read to extract the version number of a library/binary. As shown in the Figure \ref{fig:stackdbmm}, stackdb process memory model consists of an address space of a process which is divided into \emph{regions} and \emph{regions} are divided into \emph{ranges}. \emph{range} corresponds to a contiguous region of memory with same permission flags. \emph{region} is a group of related \emph{ranges}. For example, a memory mapped library file(example: \emph{glibc}) corresponds to a \emph{region} and each segment(text, data and bss) of a mapped file corresponds to a range. An entire address space content of a process is dumped by walking through regions and each of the ranges in the regions. 

\end{itemize}

Following are the APIs used in \emph{initialization}, \emph{Process Lookup} and \emph{Information Extraction} 
\begin{itemize}
\item \texttt{target\_open()} \emph{stackdb} API to open the target given target spec structure.

\item \texttt{vmi\_version\_scanner()} 
\begin{flushleft} \emph{input} : target structure, version scanner arguments\end{flushleft}
\begin{flushleft} \emph{output} : returns success on successful scanning of virtual memory pages of requested process, else returns failure code\end{flushleft}
\begin{flushleft} \emph{description} : dump virtual memory content of requested process in specified log file. Log file and process are specified in argument structure \end{flushleft}

	\begin{itemize}
		\item \texttt{target\_lookup\_sym()} call it to get the \texttt{struct bsymbol} corresponding to \texttt{init\_task}, which is the head of doubly linked list of \texttt{task\_struct}
        
        \item \texttt{os\_linux\_list\_for\_each\_struct()} used to traverse through the linked list of \texttt{task\_struct}. \texttt{gather\_version\_scanner\_info()} callback function is passed as an argument, which will be called on each \texttt{task\_struct}
        
        \item \texttt{gather\_version\_scanner\_info()} handler function called for each \texttt{task\_struct}. It invokes \texttt{version\_scanner()} to dump the virtual memory contents of the interested process.
        
        \item \texttt{version\_scanner(target, vscanner\_args)}  Entire address space of a process is divided into number of regions. Each region is divided into number of ranges. \texttt{version\_scanner()} walks through each of the ranges of the whole address space and dump the contents into a given file. 
        
	\end{itemize}

\end{itemize}
\end{enumerate}



\subsection{Limitations}
\begin{enumerate}
\item \emph{false positives} If some patches are applied to a binary/library that do not bump the version number to fix a particular vulnerability, VmiCVS does not detect that.

\item It is assumed that VM admins do not try to hide the version of binaries and libraries. That is VM admins do not morph the binaries and libraries to have different version than their original version. 

\item certain softwares such as \emph{libz}, \emph{libpam} do not have their version string encoded either in their names or in their binary code. So it is not possible to obtain the version of such softwares using our \emph{VmiCVS}
\end{enumerate}


\section{Evaluation and Results} 
\label{evaluation}
The main goals of our evaluation of VmiCVS are 
\begin{enumerate}
\item To show the extraction of version number of system libraries which are not directly exposed to remote user through any protocols
\item To show the version number extraction of service or daemon binaries
\item Quantifying the disruption caused during version number scanning
\item Comparison of \emph{Name Based} and \emph{Virtual Memory} based Version Scanners
\end{enumerate}
\par

We have not considered \emph{version parser} program for evaluation as it is a simple pattern matching script. \par

We ran our Cloud Vulnerability Scanner, VmiCVS on emulab's \emph{d820} machine. It consists of Four 2.2 GHz 64-bit 8-Core E5-4620 Sandy Bridge processors with 7.20 GT/s bus speed, 16 MB cache and VT (VT-x, EPT, and VT-d) support. Memory includes 128 GB 1333 MHz DDR3 RAM (8 x 16GB modules) and 250GB 7200 rpm SATA disk, 6 x 600GB 10000 rpm SAS disks. It was running host operating system as Ubuntu 15.04 with kernel $3.19.0-16-generic$. Virtual Machine had Ubuntu 14.04.3 LTS with $3.8.0-34-generic$ kernel. \par

\subsection{Version Number Extraction}
We have chosen three different categories of software to verify and analyze our VmiCVS version scanners. In this section, we present the version number extraction of these three different pieces of software

\begin{itemize}
\item \emph{libraries that contain version number in their filenames}
As explained in the section \ref{nameBasedVS}, most of the shared libraries contain version number in their file names. Cyrus SASL libraries required by OpenLDAP's \emph{slapd} and  Linux PAM library needed by \emph{sshd} contain version numbers in their file names. We extracted the version numbers of these libraries using \emph{Name Based Version Scanners}. \par

\textbf{SASL libraries} In our setup, Virtual Machine was running \emph{slapd} linked to $2.0.25$ version of the SASL libraries ;\emph{liblogin.so$.2.0.25$}, \emph{libplain.so$.2.0.25$}, \emph{libdigestm}\-\emph{d5.so} \-\emph{$.2.0.25$}, \emph{libsasldb.so$.2.0.25$}, \emph{libsasl.so$.2.0.25$}. \emph{Name Based version Scanner} dumped all the libraries linked to \emph{slapd} by taking \emph{slapd} as input as shown in the Figure \ref{fig:sldap}.

\begin{figure*}
\includegraphics[width=\linewidth,trim=0.0cm 0cm 0.5cm 0cm,clip]{images/sldap.jpg}
\caption{Name Based Version Scanner's version data for \emph{slapd}}
\label{fig:sldap}
\end{figure*}

\textbf{Linux PAM libraries} Similarly, we ran version scanner to extract the version of \emph{libpam} linked to \emph{sshd} binary. It dumped all the shared libraries linked to \emph{sshd} process.

\item \emph{libraries that do not contain version in their filename} In our setup, VM was running \emph{init} process linked to \emph{libc} library which had no version number in its filename. VmiCVS \emph{Virtual Memory Based Version Scanner} dumped all the virtual memory pages of \emph{init} into a file. It had the following string which contains version information :

\texttt{GNU C Library (Ubuntu EGLIBC 2.19-0ubuntu6.6) \-stable release version 2.19, by Roland McGrath\- et al}

\emph{Version Parser} reported 2.19 from the above string.

\item \emph{service/daemon binaries} We chose two different kinds of daemons 
\begin{enumerate*}[label={\alph*)}]
\item remote user facing daemons such as \emph{sshd} and  \emph{slapd}
\item internal daemons such as \emph{rsyslogd} and \emph{dnsmasq}.
\end{enumerate*}
\emph{Virtual Memory Based Version Scanner} dumped the virtual memory pages of these processes into a file.

Main string in virtual memory dumped data file that contains the version numbers is shown below for the above chosen daemons
\begin{description}%[align=cente,labelwidth=3cm]
\item [sshd] \texttt{OpenSSH\_6.6.1p1 Ubuntu-2ubuntu2.6}
\item [slapd] \texttt{buildd@lgw01-53:/build/openldap-2QUgtL/op\-enldap-2.4.31/debian/build/servers/slapd}

\item [dnsmasq] \texttt{Dnsmasq version 2.68  Copyright (c) 2000-2013 Simon Kelley}

\item [rsyslogd] \texttt{rsyslogd \%s, compiled with:}
\end{description}

\begin{comment}
\begin{verbatim}
GNU C Library (Ubuntu EGLIBC 2.19-0ubuntu6.6) stable release version 2.19, by Roland McGrath et al.
Copyright (C) 2014 Free Software Found\-ation, Inc.
This is free software; see the source for copying conditions.
There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.
Compiled by GNU CC version 4.8.2.
Compiled on a Linux 3.13.11 system on 2015-02-25.
Available extensions:
        crypt add-on version 2.1 by Michael Glad and others
        GNU Libidn by Simon Josefsson
        Native POSIX Threads Library by Ulrich Drepper et al
        BIND-8.2.3-T5B
libc ABIs: UNIQUE IFUNC
For bug reporting instructions, please see:
<https://bugs.launchpad.net/ubuntu/+source/eglibc/+bugs>
\end{verbatim}
\end{comment}

\end{itemize}

Version number of any the shared library or daemon binary can be extracted using VmiCVS version scanners. 

\subsection{Disruption}
Virtual Machine needs to be paused during version scanning. As described in the section \ref{architecture} and \ref{implementation}, version scanners consist of four phases;
\begin{enumerate*}[label={\alph*)}]
\item \emph{Initialization}
\item \emph{Process Lookup}
\item \emph{Overlay instantiation}
\item \emph{Information Extraction}
\end{enumerate*}

Virtual Machine needs to be paused during \emph{Process Lookup}, \emph{Overlay instantiation} and \emph{Information Extraction} phases. \textbf{\emph{Name Based Version Scanner}} does not need to intantiate process overlay.  



\begin{itemize}
\item \textbf{\emph{Virtual Memory based Version Scanner}} Total VM pause time and time breakdown for the processes \emph{sshd}, \emph{init}, \emph{syslogd} and \emph{dnsmasq} is shown the Table \ref{tab:virtualMemoryScanner}. It shows that Virtual Machine needs to be paused for atleast \texttt{3 seconds} during version scanning. Process Overlay instantiation phase contributes maximum share to VM pause time. This can be reduced by re-engineering the stackdb implementation to instantiate the overlay from base target. Also as shown in the Table \ref{tab:fileSize}, size of the logfile(virtual memory pages content dump) is in the order of Mega Bytes and hence Information Extraction phase takes milliseconds to dump the memory pages. This can be optimized by dumping the memory pages to fast File Systems such as \emph{/shm}.


\begin{table*}
\centering
\begin{tabular}{ |l|l|l|l|l| }
\hline
\multirow{2}{*}{\texttt{\textbf{Version Scanner Steps}}} &   \multicolumn{4}{|c|}{\texttt{Run Time in Seconds}} \\ \cline{2-5}
& \textbf{sshd} & 
\textbf{init}  & 
\textbf{dnsmasq}  & 
\textbf{rsyslogd}  \\ \hline

Initialization &   
24.1872408 & 
24.2865269 & 
24.2927834 & 
24.3372942 \\ 

{Process lookup} &    
$1.1961912 \times 10 ^{-3}$  &  
$0.0892832 \times 10 ^{-3}$  & 
$1.2629867 \times 10 ^{-3}$ & 
$0.5759090 \times 10 ^{-3}$\\ 

Overlay Instantiation &  
$3222.9633098 \times 10 ^{-3}$ &
$3215.5884553 \times 10 ^{-3}$ & 
$3167.8493740 \times 10 ^{-3}$ & 
$3320.4500295 \times 10 ^{-3}$ \\ 

Information Extraction &  
$85.2663567 \times 10 ^{-3}$ &
$60.0172360 \times 10 ^{-3}$ & 
$46.0040190 \times 10 ^{-3}$ & 
$251.011499 \times 10 ^{-3}$ \\ \hline

\rowcolor{lightgray} Total time VM Paused &  
$3309.4258577 \times 10 ^{-3}$ & 
$3275.6949745 \times 10 ^{-3}$ & 
$3215.1163797 \times 10 ^{-3}$ &  
$3572.0374375 \times 10 ^{-3}$\\
\hline
\end{tabular}
\caption{Virtual Memory based Version Scanner Run Time Break down}\label{tab:virtualMemoryScanner}
\end{table*}




\begin{table}
\centering
\begin{tabular}{ |l|l|l| }
\hline
\textbf{process} & \textbf{logfile size in bytes} \\ \hline
\emph{init} & 2,912,967 \\ \hline
\emph{dnsmasq} & 1,544,569 \\ \hline
\emph{rsyslogd} & 4,142,067 \\ \hline
\emph{sshd}  &  3,138,302 \\ \hline
\end{tabular}
\caption{Virtual memory pages dump Logfile size of processes}\label{tab:fileSize}
\end{table}

%\FloatBarrier
%\afterpage{\clearpage}
\item \textbf{\emph{Name based Version Scanner}}
Total VM pause time and time breakdown for this scanner for the processes \emph{sshd}, \emph{init}, \emph{syslogd} and \emph{dnsmasq} is shown the Table \ref{tab:nameScanner}. It shows that VM pause time is in the order of \emph{milliseconds}. Also from the Table \ref{tab:nameFileSize}, it is evident that this scanner requires less storage to dump shared library file names.


\begin{table}
\centering
\begin{tabular}{ |l|l|l| }
\hline
\textbf{process} & \textbf{logfile size in bytes} \\ \hline
\emph{init} & 404 \\ \hline
\emph{dnsmasq} & 409 \\ \hline
\emph{rsyslogd} & 406 \\ \hline
\emph{sshd}  &  663 \\ \hline
\end{tabular}
\caption{shared library file names Logfile size of processes}\label{tab:nameFileSize}
\end{table}





\begin{table*}
\centering
\begin{tabular}{ |l|l|l|l|l| }
\hline
\multirow{2}{*}{\texttt{\textbf{Version Scanner Steps}}} &   \multicolumn{4}{|c|}{\texttt{Run Time in Seconds}} \\ \cline{2-5}
& \textbf{sshd} & \textbf{init}  & \textbf{dnsmasq}  & \textbf{rsyslogd}  \\ \hline
Initialization &   
24.0563751 & 
24.1731911 & 
24.1521461 & 
24.2465212 \\ 

{Process lookup} &    
$2.0037098 \times 10 ^{-3}$  &  
$0.0668699 \times 10 ^{-3}$  & 
$1.6963645 \times 10 ^{-3}$ & 
$0.7619766 \times 10 ^{-3}$\\ 

Information Extraction &  
$3.3763275 \times 10 ^{-3}$ & 
$0.0547825 \times 10 ^{-3}$ & 
$2.2764159\times 10 ^{-3}$ & 
$2.7613869 \times 10 ^{-3}$ \\ \hline

\rowcolor{lightgray} Total time VM Paused &  
$5.3800373 \times 10 ^{-3}$ & 
$0.1216525 \times 10 ^{-3}$ & 
$3.9727796 \times 10 ^{-3}$ &  
$3.5233628 \times 10 ^{-3}$\\
\hline
\end{tabular}
\caption{Name based Version Scanner Run Time Break down}\label{tab:nameScanner}
\end{table*}
\end{itemize}


\subsection{Comparison of VmiCVS version scanners}
\begin{itemize}
\item \emph{Disruption} It is measured by Virtual Machine pause time during version scanning. As shown in the Tables \ref{tab:virtualMemoryScanner} and \ref{tab:nameScanner}, \emph{Virtual Memory Based Version Scanner} cause more disruption than \emph{Name based Version Scanner} while pausing VM in the order of \emph{seconds}.

\item \emph{Storage} \emph{Name based Version Scanner} consumes less storage space.

\item \emph{Virtual Memory Based Version Scanner} can be used to scan the version of both the shared libraries and process binaries.

\end{itemize}



\section{Conclusions and Future Work}
\label{conclusion}

VmiCVS performs version analysis through VMI technique and reports the system vulnerabilities. It produces false positives when patches are applied to fix some vulnerabilities. Not all the libraries and binaries have their release version coded. In such cases, VmiCVS fails to report the vulnerabilities.These  limitations of the VmiCVS can be overcome by using code measurement or binary analysis techniques to confirm the presence of vulnerabilities. 

Though there exists online tools~\cite{vulnList} to list vulnerabilities corresponding to particular version of a software, it is necessary to link such tools to VmiCVS version scanners to list vulnerabilities corresponding to software version.

%\FloatBarrier
\section{Acknowledgments}
We thank David M Johnson and Richard Li for their help on stackdb. We thank Prashanth Nayak for his indirect help. We have followed shared library listing technique(Name Based version Scanner) from his thesis work ~\cite{Nayak14Detecting}. We also thank Jacobus (Kobus) Van der Merwe, Eric Eide, Anton Burtsev and Feifei Li for suggesting improvements.

\bibliographystyle{abbrv}
\bibliography{sigproc}  % sigproc.bib is the name of the Bibliography in this 

\end{document}
