def is_file_detail(line):
    if ".log" in line and "vscanner_data" in line:
        return True
    return False

def runtime(logfile):
    process = None
    stats = {}
    #stats[process] = {}

    with open(logfile) as file:
        for line in file:
            print "processing - ", line
            
            if is_file_detail(line):
                key = line.split()[-1]
                print line.split()
                print line.split()[4]
                if key in stats:
                    stats[process][key].append(float(line.split()[4]))
                else:
                    stats[process][key] = [float(line.split()[4])]

            elif ("=" not in line or "." not in line) and ("process_name" not in line):
                print "line does not have [=] or [.]", line 
                continue
            
            else:
                key = (line.split("=")[0]).split()[-1]
                value = (line.split("=")[1]).split()[0]

                print "key = ", key, "value = ", value
                
                if "process_name" == key.strip():
                    process = value.strip()
                    if process not in stats:
                        stats[process] = {}
                elif key.strip() not in stats[process]:
                    stats[process][key.strip()] = [ float(value.strip()) ]
                else:
                     stats[process][key.strip()].append( float(value.strip()) )

    return stats

def statistics(stats):
    print stats
    for proc in stats:
        print ""
        print "processing : ", proc
        #print stats[proc]
        for key in stats[proc]:
            value =  stats[proc][key]
            #print key
            #print value
            avg = float(sum(value)) / len(value)
            print key, "    &   ", avg, "\\\\", "\hline"

stats = runtime("scanner_time.logs")
statistics(stats)


