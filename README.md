Shared Library Detector (ShDetector)

It detects and reports the version of the particular target library, linked to a program running inside a VM from outside VM.
This is particularly useful for the cloud provider/administrators to report the vulnerable library or services running in their cloud.

- Cloud provider can provide this service with additional charges to their tenants
- By running this program targetting a particular program running inside the VM, admin can read the version number of all the linked libraries


NOTE : It is really painful and time consuming for me to setup the vm using qemu-kvm, that works with stackdb.
       I am listing out all the steps here so that it will useful for somebody who will work with stackdb.

       Some of the problems that I faced, which motivated me to document this :
	
	- Not able to get access to serial console when VM is created with QEMU-KVM
	    o	Use libvirt - A VM management tool
	
	- When VM is created with an image available online, How do I know the user credential ?
	  Without logging into the OS, no way for me to create new user, but I need user credential to log into the device
	    o	use ubuntu-vm-builder (https://help.ubuntu.com/community/KVM/CreateGuests)
		* There is an option to specify to user name and password while creating VM image
	    o   use virt-manager to install image  

	- Successfully created VM with "virsh define/start vm1", but there is no connectivity between guest and host
	    o	Tried NAT configuration, but I was not successful in getting connectivity between Guest and Host


    1. How to install/create VM using KVM-QEMU ?
	
	- Install necessary libvirt tools and create required user accounts with proper groups
	    sudo apt-get install qemu-kvm libvirt-bin
	    sudo apt-get install virtinst
	    useradd libvirt-qemu
	    groupadd libvirt
	    usermod -a -G libvirtd,libvirt libvirt-qemu

	- For centos for Guest, download one of the image from 
	    http://isoredirect.centos.org/centos/7/isos/x86_64/CentOS-7-x86_64-Everything-1503-01.iso

	-  sudo apt-get install virt-viewer
	   sudo apt-get install virt-manager

	-  ssh into your machine with "-X" option and run "virt-manager &"
	
	- A pop-up will apprear. Fill all the required fields as required 
	    o Important field : This is where one has to activate "ethernet" option to activate networking in Guest OS
	    o Configure the required IP, DHCP range etc using this tool

    NOTE : usefull links
	* Creating a NAT Virtual Network
	    http://wiki.libvirt.org/page/TaskNATSetupVirtManager 
	* Creating VM
	    https://www.centos.org/docs/5/html/5.2/Virtualization/sect-Virtualization-Installing_guests-Create_a_guest_using_virt_manager.html
	* Installing KVM Guests
	    https://www.howtoforge.com/installing-kvm-guests-with-virt-install-on-ubuntu-8.10-server



    2. How to pass arguments to qemu for remote debugging ?
	
	- Using steps mentioned in the previous section (1), one can create the VM. But there seems to be no methods for passing arguments to 
	  qemu with virt-manager. 
	- Arguments can be passed using virsh
	- Define arguments in XML file

	* For Network connectivity, use virt-manager
	* For creating VM, use virsh	

    3. Passing arguments to qemu :
	  <qemu:commandline>
	    <qemu:arg value='-gdb'/>
	    <qemu:arg value='tcp:127.0.0.1:1234,nowait,nodelay,server'/>
	    <qemu:arg value='-qmp'/>
	    <qemu:arg value='tcp:127.0.0.1:1235,nowait,nodelay,server'/>
	    <qemu:env name='QEMU_MEMPATH_PREFIX' value='/dev/hugepages/'/>
	    <qemu:env name='LD_PRELOAD' value='/mnt/extra/akumarkk/vmi.obj/target/.libs/libqemuhacks.so.0.0.0'/>
	</qemu:commandline>

	Two tcp ports should be listening: one for gdb and oen for qmp
qemu-syst 47312          libvirt-qemu   10u     IPv4             237635        0t0        TCP localhost:1235 (LISTEN)
qemu-syst 47312 47336    libvirt-qemu   10u     IPv4             237635        0t0        TCP localhost:1235 (LISTEN)
qemu-syst 47312 47338    libvirt-qemu   10u     IPv4             237635        0t0        TCP localhost:1235 (LISTEN)
qemu-syst 47312 47706    libvirt-qemu   10u     IPv4             237635        0t0        TCP localhost:1235 (LISTEN)

root@stackdbquery:/mnt/extra# lsof | grep 1234
qemu-syst 47312          libvirt-qemu   22u     IPv4             216328        0t0        TCP localhost:1234 (LISTEN)
qemu-syst 47312 47336    libvirt-qemu   22u     IPv4             216328        0t0        TCP localhost:1234 (LISTEN)
qemu-syst 47312 47338    libvirt-qemu   22u     IPv4             216328        0t0        TCP localhost:1234 (LISTEN)


    4. Hugetlbfs : Starting ubuntu 14, kernel will have buildin hugepages support. So no need to mount hugepages separately.

    5. How to run policy_engine :
	./policy_engine --target-id 2 -t gdb --gdb-port 1234 --qemu --qemu-qmp-port 1235 --kvm -M /mnt/extra/debug/vmlinux-3.8.0-34-generic --qemu-mem-path /dev/hugepages/libvirt/qemu/qemu_back_mem.pc.ram.FkjHfz

    6. How to start virtual machine ?
	- copy *.xml config file to /etc/libvirt/qemu/ directory
	- copy *.qcow image to appropriate directory
	- Define domain: virsh define /etc/libvirt/qemu/sho_det.xml
	- open vm console using virt-manager
		o To open virt-manager, log in using SSH X-windowing system
			ssh -X pc614.emulab.net

    7. How to run sho_detector ?
	-  dpkg -i linux-image-3.8.0-34-generic-dbgsym_3.8.0-34.49~precise1_amd64.ddeb
	-  copy /usr/lib/debug/boot/vmlinux-3.8.0-34-generic to stackdb working dir (/mnt/extra/debug)
	-  cp /boot/*.3.8*  /mnt/extra/debug      // Copy /boot from VM into host's stackDB working dir
	-  ./sho_detector --target-id 3 -t gdb --gdb-port 1234 --qemu --qemu-qmp-port 1235 --kvm -M /mnt/extra/debug/vmlinux-3.8.0-34-generic --qemu-mem-path /dev/hugepages/libvirt/qemu/qemu_back_mem.pc.ram.FbWc0v 


    8. Common reasons for probe_register_line()  failure:
	-  debug version of the target is not installed in target
	   Example : sshd. If openssh-server-dbgsym is not installed, it is not possible to put watch point on this binary running inside VM.

    9. OpenVAS 
	- We have decided to compare our results with OpenVAS instead of nmap as it is considered be not a great Vulnerability Scanner.
	- To install OpenVAS, please follow the instructions from the below links
	    https://hungred.com/tag/openvas/
	    https://www.mockel.se/index.php/2015/04/openvas-8-on-ubuntu-server-14-04/

	NOTE : As of 05/03/2016, OpenVAS is not supported for ubuntu 15

