download_packages()
{
    mkdir openvas-src
    cd openvas-src/
    wget http://wald.intevation.org/frs/download.php/1638/openvas-libraries-7.0.1.tar.gz
    wget http://wald.intevation.org/frs/download.php/1640/openvas-scanner-4.0.1.tar.gz
    wget http://wald.intevation.org/frs/download.php/1637/openvas-manager-5.0.0.tar.gz
    wget http://wald.intevation.org/frs/download.php/1639/greenbone-security-assistant-5.0.0.tar.gz
    wget http://wald.intevation.org/frs/download.php/1633/openvas-cli-1.3.0.tar.gz

    tar zxfv openvas-libraries-7.0.1.tar.gz
    tar xvfz openvas-scanner-4.0.1.tar.gz
    tar xvfz openvas-manager-5.0.0.tar.gz
    tar xvfz greenbone-security-assistant-5.0.0.tar.gz
    tar xvfz openvas-cli-1.3.0.tar.gz
}

install_prereqs()
{
    sudo apt-get install build-essential bison flex cmake pkg-config  libglib2.0-dev gnutls-bin libgnutls-dev libpcap-dev libpcap0.8-dev libgpgme11 libgpgme11-dev doxygen libuuid1 uuid-dev sqlfairy xmltoman sqlite3 libxml2-dev libxslt1.1 libxslt1-dev xsltproc libmicrohttpd-dev

}

COMPS=("openvas-libraries-7.0.1" "openvas-scanner-4.0.1" "openvas-manager-5.0.0" "greenbone-security-assistant-5.0.0" "openvas-cli-1.3.0")

compile()
{
    for COMP in $COMPS
    do
	cd $COMP
	mkdir source
	cd source
	cmake ..
	make 
	sudo make install
	cd ../../
    done
}



echo "Compiling ..."
compile
